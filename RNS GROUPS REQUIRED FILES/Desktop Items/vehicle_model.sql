-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 18, 2018 at 12:41 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rns_groups`
--

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_model`
--

CREATE TABLE `vehicle_model` (
  `model_id` int(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_model`
--

INSERT INTO `vehicle_model` (`model_id`, `model_name`, `brand_id`, `vehicle_type_id`) VALUES
(1, 'Martin Rapide', 1, 2),
(2, 'A3', 2, 2),
(3, 'A4', 2, 2),
(4, 'A6', 2, 2),
(5, 'A8', 2, 2),
(6, 'Q3', 2, 4),
(7, 'Q5', 2, 4),
(8, 'Q7', 2, 4),
(9, 'S5', 2, 2),
(10, 'TT', 2, 2),
(11, '3 Series', 3, 2),
(12, '5 Series', 3, 2),
(13, 'X4', 3, 4),
(14, '6 Series', 3, 2),
(15, '7 Series', 3, 2),
(16, 'X1', 3, 4),
(17, 'X3', 3, 4),
(18, 'X5', 3, 4),
(19, 'Aveo', 4, 2),
(20, 'Aveo U VA', 4, 1),
(21, 'Beat', 4, 1),
(22, 'Captiva', 4, 4),
(23, 'Cruze', 4, 2),
(24, 'Enjoy', 4, 4),
(25, 'Optra', 4, 2),
(26, 'Optra Magnum', 4, 2),
(27, 'Sail', 4, 2),
(28, 'Spark', 4, 1),
(29, 'Tavera', 4, 2),
(30, 'GO', 5, 1),
(31, 'GO Plus', 5, 1),
(32, 'redi-GO', 5, 1),
(33, 'Grande Punto', 6, 1),
(34, 'Linea', 6, 2),
(35, 'Palio', 6, 1),
(36, 'Palio NV', 6, 1),
(37, 'Punto', 6, 1),
(38, 'Aspire', 7, 2),
(39, 'EcoSport', 7, 14),
(40, 'Endeavour', 7, 4),
(41, 'Fiesta', 7, 2),
(42, 'Fiesta Classic', 7, 2),
(43, 'Figo', 7, 1),
(44, 'Ikon', 7, 2),
(45, 'Accord', 8, 2),
(46, 'Amaze', 8, 2),
(47, 'Brio', 8, 1),
(48, 'BRV', 8, 4),
(49, 'City', 8, 2),
(50, 'City ZX', 8, 2),
(51, 'Civic', 8, 2),
(52, 'CR-V', 8, 4),
(53, 'Jazz', 8, 2),
(54, 'Mobilio', 8, 14),
(55, 'H2', 9, 4),
(56, 'Accent', 10, 2),
(57, 'Creta', 10, 14),
(58, 'Elantra', 10, 2),
(59, 'Elite i20', 10, 1),
(60, 'EON', 10, 1),
(61, 'Getz', 10, 1),
(62, 'Grand i10', 10, 1),
(63, 'i10', 10, 1),
(64, 'i20', 10, 1),
(65, 'i20 Active', 10, 1),
(66, 'Santa Fe', 10, 1),
(67, 'Santro', 10, 1),
(68, 'Santro Xing', 10, 1),
(69, 'Sonata', 10, 2),
(70, 'Sonata Embera', 10, 2),
(71, 'Sonata Transform', 10, 2),
(72, 'Verna', 10, 2),
(73, 'Xcent', 10, 2),
(74, 'XF', 11, 2),
(75, 'XJ', 11, 2),
(76, 'Compass', 12, 4),
(77, 'Rover Discovery Sport', 13, 4),
(78, 'Rover Freelander 2', 13, 4),
(79, 'Rover Range Rover Evoque', 13, 4),
(80, 'Rover Range Rover Sport', 13, 4),
(81, 'Rover Range Rover', 13, 4),
(82, 'Bolero', 14, 4),
(83, 'Jeep', 14, 4),
(84, 'KUV', 14, 14),
(85, 'Logan', 14, 2),
(86, 'Quanto', 14, 14),
(87, 'Renault Logan', 14, 2),
(88, 'Scorpio', 14, 4),
(89, 'Ssangyong Rexton', 14, 4),
(90, 'TUV 300', 14, 14),
(91, 'Verito', 14, 2),
(92, 'XUV500', 14, 4),
(93, 'Xylo', 14, 4),
(94, '800', 15, 1),
(95, 'A Star', 15, 1),
(96, 'Alto', 15, 1),
(97, 'Alto 800', 15, 1),
(98, 'Alto K10', 15, 1),
(99, 'Baleno', 15, 1),
(100, 'Celerio', 15, 1),
(101, 'Ciaz', 15, 2),
(102, 'Dzire', 15, 2),
(103, 'Eeco', 15, 14),
(104, 'Ertiga', 15, 14),
(105, 'Esteem', 15, 2),
(106, 'Grand Vitara', 15, 4),
(107, 'Ignis', 15, 1),
(108, 'Kizashi', 15, 2),
(109, 'Omni', 15, 14),
(110, 'Ritz', 15, 1),
(111, 'S-Cross', 15, 1),
(112, 'Swift', 15, 1),
(113, 'SX4', 15, 2),
(114, 'Versa', 15, 14),
(115, 'Vitara Brezza', 15, 14),
(116, 'Wagon R', 15, 1),
(117, 'Wagon R Stingray', 15, 1),
(118, 'Zen', 15, 1),
(119, 'Zen Estilo', 15, 1),
(120, 'Quattroporte', 16, 2),
(121, 'A-Class', 17, 2),
(122, 'B-Class', 17, 2),
(123, 'C-Class', 17, 2),
(124, 'CLA', 17, 2),
(125, 'CLS', 17, 2),
(126, 'E-Class', 17, 2),
(127, 'GLA Class', 17, 1),
(128, 'GL-Class', 17, 4),
(129, 'GLE', 17, 4),
(130, 'MB Class', 17, 4),
(131, 'M-Class', 17, 4),
(132, 'S-Class', 17, 2),
(133, 'SLC', 17, 2),
(134, 'Viano', 17, 4),
(135, 'Cooper', 18, 1),
(136, 'Cooper 5 DOOR', 18, 1),
(137, 'Cooper Convertible', 18, 1),
(138, 'Countryman', 18, 1),
(139, 'Cedia', 19, 1),
(140, 'Montero', 19, 1),
(141, 'Pajero Sport', 19, 1),
(142, 'Evalia', 20, 14),
(143, 'Micra', 20, 1),
(144, 'Sunny', 20, 2),
(145, 'Teana', 20, 2),
(146, 'Terrano', 20, 4),
(147, 'Astra', 21, 1),
(148, 'Cayenne', 22, 4),
(149, 'Duster', 23, 4),
(150, 'Fluence', 23, 2),
(151, 'KWID', 23, 1),
(152, 'Lodgy', 23, 14),
(153, 'Pulse', 23, 1),
(154, 'Scala', 23, 2),
(155, 'Fabia', 24, 1),
(156, 'Laura', 24, 2),
(157, 'Octavia', 24, 2),
(158, 'Rapid', 24, 1),
(159, 'Superb', 24, 2),
(160, 'Yeti', 24, 1),
(161, 'Aria', 25, 4),
(162, 'Bolt', 25, 1),
(163, 'Hexa', 25, 4),
(164, 'Indica', 25, 1),
(165, 'Indica eV2', 25, 1),
(166, 'Indica V2', 25, 1),
(167, 'Indica V2 Xeta', 25, 1),
(168, 'Indica Vista', 25, 1),
(169, 'Indigo', 25, 2),
(170, 'Indigo eCS', 25, 2),
(171, 'Indigo Marina', 25, 14),
(172, 'Indigo XL', 25, 2),
(173, 'Manza', 25, 2),
(174, 'Nano', 25, 1),
(175, 'Safari', 25, 2),
(176, 'Safari Storme', 25, 2),
(177, 'Sumo Victa DI', 25, 2),
(178, 'Tiago', 25, 1),
(179, 'Vista', 25, 1),
(180, 'Zest', 25, 2),
(181, 'Alphard', 26, 4),
(182, 'Camry', 26, 2),
(183, 'Corolla', 26, 2),
(184, 'Corolla Altis', 26, 2),
(185, 'Etios Cross', 26, 2),
(186, 'Etios Liva', 26, 1),
(187, 'Fortuner', 26, 4),
(188, 'Innova', 26, 4),
(189, 'Innova Crysta', 26, 4),
(190, 'Land Cruiser Prado', 26, 4),
(191, 'Platinum Etios', 26, 2),
(192, 'Qualis', 26, 4),
(193, 'Ameo', 27, 2),
(194, 'Beetle', 27, 1),
(195, 'CrossPolo', 27, 14),
(196, 'Jetta', 27, 2),
(197, 'Passat', 27, 2),
(198, 'Polo', 27, 1),
(199, 'Vento', 27, 2),
(200, 'S60', 28, 2),
(201, 'XC60', 28, 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  ADD PRIMARY KEY (`model_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
