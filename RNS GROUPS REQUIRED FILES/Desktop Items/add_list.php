<?php
	include_once("config.php");
	include_once("insert_business.php");
	include_once("insert_individual.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	 
	<title>Add Listing | Labour Naka</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="AxilBoard Bootstrap 4 Admin Template">
	<meta name="author" content="CodePassenger">
	
	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=css?family=Poppins%7cMaterial+Icons" rel="stylesheet" type='text/css'>
	
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/bootstrap-formhelpers.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/bootstrap-slider.min.css">
    <link rel="stylesheet" href="assets/css/uploadfile.css">
    <link rel="stylesheet" href="assets/css/emoji.css">
    <link rel="stylesheet" href="assets/css/fullcalendar.min.css">
    <link rel="stylesheet" href="assets/css/lobipanel.min.css">
	
	<!-- Material Design CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="assets/css/ripples.min.css">
	<link rel="stylesheet" href="assets/css/mdb.min.css">
	
	<!-- Responsive Mobile Menu -->
	<link rel="stylesheet" href="assets/css/responsive-menu/jquery.accordion.css">
	<link rel="stylesheet" href="css/vertical-menu.css">
	
	<!-- Data Table CSS -->
	<link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="assets/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="assets/css/select.dataTables.min.css">
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
	<!-- Vector-ammap CSS -->
	<link rel="stylesheet" href="assets/css/ammap.css">
	
    <link rel="stylesheet" href="css/app.css">
	
    <link rel="stylesheet" href="css/responsive.css">
	<script type="text/javascript">
		window.onload = function() {
			document.getElementById('business_form').style.display = 'none';
			document.getElementById('individual_form').style.display = 'none';
		}
		function yesnoCheck() {
			if (document.getElementById('r1').checked) {
				document.getElementById('business_form').style.display = 'block';
				document.getElementById('individual_form').style.display = 'none';
			} 
			else if(document.getElementById('r2').checked) {
				document.getElementById('individual_form').style.display = 'block';
				document.getElementById('business_form').style.display = 'none';
		   }
		}	
    </script>
</head>
<body class="menu-collapsed">
	<?php include_once("header.php"); ?>
	<!-- apps-header -->
	<?php include_once("aside.php"); ?>
	<!--aside bar-->
	<div class="apps-container-wrap page-container">
		<div class="page-content">
			<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="box-widget">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="panel-title">
										<h4>Business Details</h4>
									</div>
								</div>
								<div class="panel-body">
									<div class="form-block">
										<form class="form-common" method="POST" action="">
											<div class="row">
												<div class="col-lg-6">
													<h5>Whether Business/Individual?</h5>
													<div class="form-group form-check-inline">
														<label class="custom-control custom-radio">
															<input id="r1" name="radio" value="0" type="radio" class="custom-control-input" onclick="javascript:yesnoCheck();"/>
															<span class="custom-control-indicator"></span>
															<span class="custom-control-description">Business</span>
														</label>
													</div>
													<div class="form-group form-check-inline">
														<label class="custom-control custom-radio">
															<input id="r2" name="radio" value="1" type="radio" class="custom-control-input" onclick="javascript:yesnoCheck();"/>
															<span class="custom-control-indicator"></span>
															<span class="custom-control-description">Individual</span>
														</label>
													</div>
													<div class="form-group">
														<input id="b_i" name="submit" type="submit" class="custom-control-input"/>
													</div>
												</div>
											</div>
										</form>
										<form id="business_form" style="display:none;" class="form-common" method="POST" action="" enctype="multipart/form-data">
											<div class="row">
												<div class="col-lg-6">
													<h4>Business Information</h4><br/>
												</div>
												<div class="col-lg-6">
													<h4>Contact Information</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label for="listing_name">Listing Name</label>
														<input type="text" name="listing_name" class="form-control" id="listing_name" placeholder="Business Name*" required>
														<input type="hidden" name="tab" value="0" class="form-control" id="radio">
														<input type="hidden" name="show1" value="1" class="form-control" id="radio">
													</div>
													<div class="form-group">
														<label for="tagline">Business Tagline</label>
														<input type="text" name="tagline" class="form-control" id="tagline" placeholder="Business Tagline">
													</div>
													<div class="form-group">
														<label for="location">Location</label>
														<input type="text" name="location" class="form-control" id="location" placeholder="Location*" required>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label for="contact">Contact Number</label>
														<input type="text" name="contact" class="form-control" id="contact" placeholder="Contact Number*" required>
													</div>
													<div class="form-group">
														<label for="email">Contact Email</label>
														<input type="email" name="email" class="form-control" id="email" placeholder="Email Address*" required>
													</div>
													<div class="form-group">
														<label for="address">Address</label>
														<input type="text" name="address" class="form-control" id="address" placeholder="Address*" required>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Other Information</h4><br/>
												</div>
												<div class="col-lg-6">
													<br/><h4>Social Profiles</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label for="category">Category*</label><br/>
														<select id="listing_category" name="listing_category" class="custom-select" onchange="showsubcat(this.value);" style="width:500px !important;" required>
															<option>Select Category</option>
															<?php while($row_q=$res_q->fetch_assoc()) {?>
															<option value="<?php echo $row_q["category_id"]; ?>"><?php echo $row_q["category_name"]; ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<label for="sub_category">Sub-Category*</label><br/>
														<select id="listing_sub_category" name="listing_sub_category" class="custom-select" style="width:500px !important;" required>
															<option>Select Category First</option>
														</select>
													</div>
													<div class="form-group">
														<label for="city">City*</label><br/>
														<select id="listing_city" name="listing_city" class="custom-select" onchange="showLocal(this.value)" style="width:500px !important;" required>
															<option>Select City*</option>
															<?php while($row_w=$res_w->fetch_assoc()) {?>
																<option value="<?php echo $row_w["city_id"]; ?>"><?php echo $row_w["city_name"]; ?></option>
															<?php }?>
														</select>
													</div>
													<div class="form-group">
														<label for="local_area">Local Area*</label><br/>
														<select id="listing_area" name="listing_area" class="custom-select" style="width:500px !important;" required>
																<option>Select City First</option>
														</select>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label for="website">Website</label>
														<input type="text" name="website" class="form-control" id="website" placeholder="Business Website*" required>
													</div>
													<div class="form-group">
														<label for="facebook">Facebook</label>
														<input type="text" name="facebook" class="form-control" id="facebook" placeholder="Facebook URL">
													</div>
													<div class="form-group">
														<label for="googleplus">Google+</label>
														<input type="text" name="googleplus" class="form-control" id="googleplus" placeholder="Google+ URL">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Describe your Business</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<label for="description">Description</label>
														<textarea rows="5" name="description" cols="50" id="description" class="form-control" placeholder="Description*" required></textarea>
													</div>
													<div class="form-group">
														<label for="keywords">Keywords</label>
														<input type="text" name="keywords" class="form-control" id="keywords" placeholder="Keywords">
													</div>
													
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Upload Images</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div style="font-size:14px; width:100%;">
														<label for="company_logo" class="col-md-3 col-form-label left-align">Company Logo</label>
														<input type="file" name="company_logo" id="company_logo">
													</div>
													<div style="font-size:14px; width:100%; margin-top:20px;">
														<label for="category_image" class="col-md-3 col-form-label">Category Image</label>
														<input type="file" name="category_image" id="category_image">
													</div>
													<div style="font-size:14px; width:100%;">
														<label for="sub_category_image" class="col-md-3 col-form-label left-align">Sub-Category Image</label>
														<input type="file" name="sub_category_image" id="sub_category_image">
													</div>
												</div>
												<div class="col-lg-6">
													<div style="font-size:14px; width:100%;">
														<label for="cover_image" class="col-md-3 col-form-label left-align">Cover Image</label>
														<input type="file" name="cover_image" id="cover_image">
													</div>
													<div style="font-size:14px; width:100%;">
														<label for="bg_image" class="col-md-3 col-form-label left-align">Background Image</label>
														<input type="file" name="bg_image" id="bg_image">
													</div>
													<div style="font-size:14px; width:100%; margin-top:20px;">
														<label for="gallery_image" class="col-md-3 col-form-label left-align">Gallery Image</label>
														<input type="file" name="gallery_image" id="gallery_image">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
												</div>
												<div class="col-lg-4">
													<div class="form-btn-block">
														<button type="submit" name="business" class="btn btn-raised btn-primary waves-effect waves-light">Submit</button>
														<button type="reset" name="reset" class="btn btn-outline-default btn-xs btn-raised waves-effect">Reset</button>
													</div>
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</form>
										<form id="individual_form" class="form-common" method="POST" action="" enctype="multipart/form-data" style="display:none;">
											<div class="row">
												<div class="col-lg-6">
													<h4>Individual Information</h4><br/>
												</div>
												<div class="col-lg-6">
													<h4>Contact Information</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label for="person_name">Name*</label>
														<input type="text" name="individual_name" class="form-control" id="individual_name" placeholder="Person Name*" required>
														<input type="hidden" name="tab1" value="1" class="form-control" id="radio">
														<input type="hidden" name="show2" value="1" class="form-control" id="radio">
													</div>
													<div class="form-group">
														<label for="designation">Designation*</label>
														<input type="text" name="designation" class="form-control" id="designation" placeholder="Designation">
													</div>
													<div class="form-group">
														<label for="location">Location*</label>
														<input type="text" name="individual_location" class="form-control" id="individual_location" placeholder="Location*" required>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label for="contact">Contact Number*</label>
														<input type="text" name="individual_contact" class="form-control" id="individual_contact" placeholder="Contact Number*" required>
													</div>
													<div class="form-group">
														<label for="in_email">Contact Email*</label>
														<input type="email" name="individual_email" class="form-control" id="individual_email" placeholder="Email Address*" required>
													</div>
													<div class="form-group">
														<label for="address">Address*</label>
														<input type="text" name="individual_address" class="form-control" id="individual_address" placeholder="Address*" required>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Other Information</h4><br/>
												</div>
												<div class="col-lg-6">
													<br/><h4>Social Profiles</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label for="category">Category*</label><br/>
														<select id="individual_listing_category" name="individual_listing_category" class="custom-select" onchange="showsubcatin(this.value)" style="width:500px !important;" required>
															<?php while($row_r=$res_r->fetch_assoc()) {?>
															<option value="<?php echo $row_r["category_id"]; ?>"><?php echo $row_r["category_name"]; ?></option>
															<?php }?>
														</select>
													</div>
													<div class="form-group">
														<label for="sub_category">Sub-Category*</label><br/>
														<select id="individual_listing_sub_category" name="individual_listing_sub_category" class="custom-select" style="width:500px !important;" required>
															<option>Select Category First</option>
														</select>
													</div>
													<div class="form-group">
														<label for="city">City*</label><br/>
														<select id="listing_city_in" name="listing_city_in" class="custom-select" onchange="showArea(this.value)" style="width:500px !important;" required>
															<option>Select City*</option>
															<?php while($row_x=$res_x->fetch_assoc()) {?>
																<option value="<?php echo $row_x["city_id"]; ?>"><?php echo $row_x["city_name"]; ?></option>
															<?php }?>
														</select>
													</div>
													<div class="form-group">
														<label for="area">Local Area*</label><br/>
														<select id="listing_area_ind" name="listing_area_ind" class="custom-select" style="width:500px !important;" required>
															<option>Select City First</option>
														</select>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label for="website">Website</label>
														<input type="text" name="individual_website" class="form-control" id="individual_website" placeholder="Business Website(optional)">
													</div>
													<div class="form-group">
														<label for="facebook">Facebook</label>
														<input type="text" name="individual_facebook" class="form-control" id="individual_facebook" placeholder="Facebook URL(optional)">
													</div>
													<div class="form-group">
														<label for="individual_googleplus">Google+</label>
														<input type="text" name="individual_googleplus" class="form-control" id="individual_googleplus" placeholder="Google+ URL(optional)">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Describe Individual</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<label for="description">Description*</label>
														<textarea rows="5" name="individual_description" cols="50" id="individual_description" class="form-control" placeholder="Description*" required></textarea>
													</div>
													<div class="form-group">
														<label for="keywords">Keywords</label>
														<input type="text" name="individual_keywords" class="form-control" id="individual_keywords" placeholder="Keywords">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<br/><h4>Attach Documents</h4><br/>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
												<div style="font-size:14px; width:100%;">
														<label for="individual_profile" class="col-md-3 col-form-label left-align">Profile Photo</label>
														<input type="file" name="individual_profile" id="individual_profile">
													</div>
													<div style="font-size:14px; width:100%;">
														<label for="individual_document" class="col-md-3 col-form-label left-align">Document Image</label>
														<input type="file" name="individual_document" id="individual_document">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
												</div>
												<div class="col-lg-4">
													<div class="form-btn-block">
														<button type="submit" name="individual" class="btn btn-raised btn-primary waves-effect waves-light">Submit</button>
														<button type="reset" name="reset" class="btn btn-outline-default btn-xs btn-raised waves-effect">Reset</button>
													</div>
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</form>
									</div>
								</div><!--panel Body -->
							</div><!--panel -->
					</div><!-- widget-module -->
				</div>
				
			</div>
				<?php include_once("footer.php");?>
			</div>
		</div><!-- page-content -->
	</div><!-- page-container -->
	<script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/tether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/bootstrap-toggle.min.js"></script>
    <script src="assets/js/bootstrap-formhelpers.min.js"></script>
    <script src="assets/js/bootstrap-formhelpers-languages.js"></script>
    <script src="assets/js/mdb.min.js"></script>
    <script src="assets/js/bootstrap-slider.min.js"></script>
	<script src="assets/js/validator.min.js"></script>
	<script src="assets/js/jquery.inputmask.bundle.min.js"></script>
	<script src="assets/js/jquery-tree-view.js"></script>
	<script src="assets/js/jquery.uploadfile.min.js"></script>
	<script src="assets/js/jquery.slimscroll.min.js"></script>
	<script src="assets/js/jquery.simpleWeather.min.js"></script>
	<script src="assets/js/tinymce/tinymce.min.js"></script>
	<script src="assets/js/fullcalendar/moment.min.js"></script>
	<script src="assets/js/fullcalendar/fullcalendar.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/lobipanel.min.js"></script>
	<script src="assets/js/jquery.steps.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	
	<!-- Material-JS -->
	<script src="assets/js/material.min.js"></script>
	<script src="assets/js/ripples.min.js"></script>
	
	<!-- Responsive Mobile Menu -->
	<script src="assets/js/responsive-menu/jquery.accordion.js"></script>
	
	<!-- Data-Table-JS -->
	<script src="assets/js/datatable/jquery.dataTables.min.js"></script>
	<script src="assets/js/datatable/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/datatable/dataTables.select.min.js"></script>
	<script src="assets/js/datatable/dataTables.buttons.min.js"></script>
	<script src="assets/js/datatable/buttons.flash.min.js"></script>
	<script src="assets/js/datatable/jszip.min.js"></script>
	<script src="assets/js/datatable/vfs_fonts.js"></script>
	<script src="assets/js/datatable/buttons.html5.min.js"></script>
	<script src="js/datatable-custom.js"></script>
	
	<!-- Chart-JS -->
    <script src="assets/js/chart/Chart.bundle.min.js"></script>
	<script src="js/chart-custom.js"></script>
	
	<!-- Counter-Up-JS -->
	<script src="assets/js/jquery.waypoints.min.js"></script>
	<script src="assets/js/jquery.counterup.min.js"></script>
	
	<!-- Emoji-JS -->
	<script src="assets/js/emoji/config.js"></script>
	<script src="assets/js/emoji/util.js"></script>
	<script src="assets/js/emoji/jquery.emojiarea.js"></script>
	<script src="assets/js/emoji/emoji-picker.js"></script>
	
	<!-- Vector-Map-Ammap-JS -->
	<script src="assets/js/ammap/ammap.js"></script>
	<script src="assets/js/ammap/worldLow.js"></script>
	<script src="assets/js/ammap/black.js"></script>
	
    <script src="js/custom.js"></script>
	
	<script>
	function showsubcat(str) {
	  var xhttp;    
	  if (str == "") {
		document.getElementById("listing_sub_category").innerHTML = "";
		return;
	  }
	  xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
		  document.getElementById("listing_sub_category").innerHTML = xhttp.responseText;
		}
	  };
	  xhttp.open("GET", "get_sub_cat.php?id="+str, true);
	  xhttp.send();
	}
	
	function showsubcatin(stri) {
	  var xhttpi;    
	  if (stri == "") {
		document.getElementById("individual_listing_sub_category").innerHTML = "";
		return;
	  }
	  xhttpi = new XMLHttpRequest();
	  xhttpi.onreadystatechange = function() {
		if (xhttpi.readyState == 4 && xhttpi.status == 200) {
		  document.getElementById("individual_listing_sub_category").innerHTML = xhttpi.responseText;
		}
	  };
	  xhttpi.open("GET", "get_sub_cat_in.php?id="+stri, true);
	  xhttpi.send();
	}
	
	function showLocal(strl) {
	  var xhttpl;    
	  if (strl == "") {
		document.getElementById("listing_area").innerHTML = "";
		return;
	  }
	  xhttpl = new XMLHttpRequest();
	  xhttpl.onreadystatechange = function() {
		if (xhttpl.readyState == 4 && xhttpl.status == 200) {
		  document.getElementById("listing_area").innerHTML = xhttpl.responseText;
		}
	  };
	  xhttpl.open("GET", "get_area.php?id="+strl, true);
	  xhttpl.send();
	}
	
	function showArea(str2) {
	  var xhttp2;    
	  if (str2 == "") {
		document.getElementById("listing_area_ind").innerHTML = "";
		return;
	  }
	  xhttp2 = new XMLHttpRequest();
	  xhttp2.onreadystatechange = function() {
		if (xhttp2.readyState == 4 && xhttp2.status == 200) {
		  document.getElementById("listing_area_ind").innerHTML = xhttp2.responseText;
		}
	  };
	  xhttp2.open("GET", "get_area_ind.php?id="+str2, true);
	  xhttp2.send();
	}
	</script>
	
	<script>
		AmCharts.theme = AmCharts.themes.black;
		AmCharts.ready(function(){
			var map = new AmCharts.AmMap();
			var dataProvider = {
				mapVar: AmCharts.maps.worldLow,
				getAreasFromMap:true,
				areas:[
					{ "id": "AU", "color": "#ff7979" },
					{ "id": "US", "color": "#83e9d2" },
					{ "id": "RS", "color": "#83e9d2" },
					{ "id": "RU", "color": "#83e9d2" },
					{ "id": "CA", "color": "#ff7979" },
					{ "id": "BR", "color": "#ffce2e" },
					{ "id": "MX", "color": "#ffce2e" },
					{ "id": "AR", "color": "#ffce2e" },
					{ "id": "CO", "color": "#ffce2e" },
					{ "id": "PE", "color": "#ffce2e" },
					{ "id": "EC", "color": "#ffce2e" },
					{ "id": "CU", "color": "#ffce2e" },
					{ "id": "HA", "color": "#ffce2e" },
					{ "id": "CL", "color": "#ffce2e" },
					{ "id": "PY", "color": "#ffce2e" },
					{ "id": "UY", "color": "#ffce2e" },
					{ "id": "BO", "color": "#ffce2e" },
					{ "id": "FK", "color": "#ffce2e" },
					{ "id": "VE", "color": "#ffce2e" },
					{ "id": "GT", "color": "#ffce2e" },
					{ "id": "HN", "color": "#ffce2e" },
					{ "id": "MX", "color": "#ffce2e" },
					{ "id": "GY", "color": "#ffce2e" },
					{ "id": "HT", "color": "#ffce2e" },
					{ "id": "SR", "color": "#ffce2e" },
					{ "id": "GF", "color": "#ffce2e" },
					{ "id": "DO", "color": "#ffce2e" },
					{ "id": "JM", "color": "#ffce2e" },
					{ "id": "CR", "color": "#ffce2e" },
					{ "id": "NI", "color": "#ffce2e" },
					{ "id": "SV", "color": "#ffce2e" },
					{ "id": "PA", "color": "#ffce2e" },
					{ "id": "BZ", "color": "#ffce2e" },
					{ "id": "SJ", "color": "#ffce2e" },
					{ "id": "NO", "color": "#ffce2e" },
					{ "id": "SE", "color": "#ffce2e" },
					{ "id": "FI", "color": "#ffce2e" },
				 ]
			};
			map.dataProvider = dataProvider;
			map.areasSettings ={
				autoZoom: true,
				rollOverBrightness:10,
				selectedBrightness:20,
				selectedColor: "#5eb7ff"
			};
			map.write("vectorWorldMap");
		});
	</script>

</body>


</html>