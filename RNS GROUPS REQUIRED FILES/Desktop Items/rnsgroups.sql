-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2018 at 02:31 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rnsgroups`
--

-- --------------------------------------------------------

--
-- Table structure for table `automobile`
--

CREATE TABLE `automobile` (
  `auto_id` int(11) NOT NULL,
  `auto_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `automobile`
--

INSERT INTO `automobile` (`auto_id`, `auto_name`) VALUES
(1, 'car'),
(2, 'transport vehicle'),
(3, 'bike'),
(4, 'auto rickshaw');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `service_id` varchar(11) NOT NULL,
  `booking_date` varchar(50) NOT NULL,
  `message` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `customer_id`, `vehicle_id`, `service_id`, `booking_date`, `message`) VALUES
(1, 00154, 15, '7,8', '09-07-2018 18:30', 'qwertyui');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(80) NOT NULL,
  `dept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(50) NOT NULL,
  `customer_name` varchar(70) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email_id` varchar(70) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `password` varchar(16) NOT NULL,
  `register_date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `username`, `customer_name`, `address`, `email_id`, `phone`, `password`, `register_date`) VALUES
(00001, 'ajish96', 'Ajish', 'C/02, Alishan CHS', 'www.ajishjoy@gmail.com', '8286541408', '6FAJS12C', ''),
(00154, 'admin', 'Ajish Joy thottungal', 'C/02 Alishan CHS', 'www.ajishjoy@gmail.com', '8286541407', 'ajish@123', ''),
(00155, 'dhawnit', 'dhawnit', 'shahad', 'demo@demo.com', '9702718586', '18HQKGUM', ''),
(00156, 'adminsss', 'Ajish', 'C/02, Alishan CHS', 'a@gmail.com', '8286578407', 'IJ4YW3H1', '20/06/2018'),
(00157, 'arken', 'arken', 'sss', 'qwert', '8286541496', 'H4O6PB58', '27/06/2018'),
(00159, 'vijay93', 'Vijay Jadhav', 'kalyan', 'email@email.com', '9594319954', '3CO6UJQL', '02/07/2018');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `rate_id` int(11) NOT NULL,
  `rate` varchar(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`rate_id`, `rate`, `vehicle_type_id`, `service_id`) VALUES
(1, '50', 1, 1),
(2, '70', 1, 2),
(3, '150', 1, 3),
(4, '200', 1, 4),
(5, '250', 1, 5),
(6, '300', 1, 6),
(7, '300', 1, 7),
(8, '400', 1, 8),
(9, '700', 1, 9),
(10, '800', 1, 10),
(11, '1500', 1, 11),
(12, '1500', 1, 12),
(13, '10000', 1, 13),
(14, '60', 2, 1),
(15, '80', 2, 2),
(16, '200', 2, 3),
(17, '250', 2, 4),
(18, '300', 2, 5),
(19, '350', 2, 6),
(20, '350', 2, 7),
(21, '450', 2, 8),
(22, '800', 2, 9),
(23, '900', 2, 10),
(24, '1800', 2, 11),
(25, '1800', 2, 12),
(26, '15000', 2, 13),
(27, '70', 3, 1),
(28, '90', 3, 2),
(29, '250', 3, 3),
(30, '300', 3, 4),
(31, '350', 3, 5),
(32, '400', 3, 6),
(33, '400', 3, 7),
(34, '500', 3, 8),
(35, '80', 4, 1),
(36, '100', 4, 2),
(37, '300', 4, 3),
(38, '350', 4, 4),
(39, '400', 4, 5),
(40, '450', 4, 6),
(41, '450', 4, 7),
(42, '550', 4, 8),
(43, '900', 4, 9),
(44, '1000', 4, 10),
(45, '2000', 4, 11),
(46, '2000', 4, 12),
(47, '20000', 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(80) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `auto_id`) VALUES
(1, 'WATER WASH', 1),
(2, 'WATER + DRY', 1),
(3, 'SHAMPOO WASH WITHOUT VACCUM', 1),
(4, 'SHAMPOO WASH WITH VACCUM', 1),
(5, 'DIESEL WASH WITHOUT VACCUM', 1),
(6, 'DIESEL WASH WITH VACCUM', 1),
(7, 'SHAMPOO WASH WITH VEHICLE LIFT', 1),
(8, 'DIESEL WASH WITH VEHICLE LIFT', 1),
(9, 'INTERIOR CLEANING', 1),
(10, 'TEFLON POLISH', 1),
(11, 'ENGINE COATING', 1),
(12, 'ANTIRUST COATING', 1),
(13, 'RNS PAINT PROTECTION', 1),
(14, 'WATER WASH', 2),
(15, 'SHAMPOO WASH WITHOUT VACCUM', 2),
(16, 'DIESEL WASH WITHOUT VACCUM', 2),
(17, 'SHAMPOO WASH WITH VEHICLE LIFT', 2),
(18, 'DIESEL WASH WITH VEHICLE LIFT', 2),
(19, 'WATER WASH', 3),
(20, 'WATER + DRY', 3),
(21, 'SHAMPOO WASH', 3),
(22, 'DIESEL WASH ', 3),
(23, 'WATER WASH', 4),
(24, 'WATER + DRY', 4),
(25, 'SHAMPOO WASH', 4),
(26, 'DIESEL WASH ', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(11) NOT NULL,
  `dept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_type_id` varchar(50) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `make_brand` varchar(50) NOT NULL,
  `vehicle_model` varchar(50) NOT NULL,
  `vehicle_number` varchar(50) NOT NULL,
  `customer_id` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `vehicle_type_id`, `auto_id`, `make_brand`, `vehicle_model`, `vehicle_number`, `customer_id`) VALUES
(3, '2', 2, 'TATA', 'Tempo 404', 'MH05 HT 7863', '00154'),
(15, '2', 1, 'Aston', 'Martin Rapide', 'qw-vj-789654', '00154');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_brand`
--

CREATE TABLE `vehicle_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_brand`
--

INSERT INTO `vehicle_brand` (`brand_id`, `brand_name`, `auto_id`) VALUES
(1, 'Aston', 1),
(2, 'Audi', 1),
(3, 'BMW', 1),
(4, 'Chevrolet', 1),
(5, 'Datsun', 1),
(6, 'Fiat', 1),
(7, 'Ford', 1),
(8, 'Honda', 1),
(9, 'Hummer', 1),
(10, 'Hyundai', 1),
(11, 'Jaguar', 1),
(12, 'Jeep', 1),
(13, 'Land', 1),
(14, 'Mahindra', 1),
(15, 'Maruti', 1),
(16, 'Maserati', 1),
(17, 'Mercedes-Benz', 1),
(18, 'Mini', 1),
(19, 'Mitsubishi', 1),
(20, 'Nissan', 1),
(21, 'Opel', 1),
(22, 'Porsche', 1),
(23, 'Renault', 1),
(24, 'Skoda', 1),
(25, 'Tata', 1),
(26, 'Toyota', 1),
(27, 'Volkswagen', 1),
(28, 'Volvo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_model`
--

CREATE TABLE `vehicle_model` (
  `model_id` int(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_model`
--

INSERT INTO `vehicle_model` (`model_id`, `model_name`, `brand_id`, `vehicle_type_id`) VALUES
(1, 'Martin Rapide', 1, 2),
(2, 'A3', 2, 2),
(3, 'A4', 2, 2),
(4, 'A6', 2, 2),
(5, 'A8', 2, 2),
(6, 'Q3', 2, 4),
(7, 'Q5', 2, 4),
(8, 'Q7', 2, 4),
(9, 'S5', 2, 2),
(10, 'TT', 2, 2),
(11, '3 Series', 3, 2),
(12, '5 Series', 3, 2),
(13, 'X4', 3, 4),
(14, '6 Series', 3, 2),
(15, '7 Series', 3, 2),
(16, 'X1', 3, 4),
(17, 'X3', 3, 4),
(18, 'X5', 3, 4),
(19, 'Aveo', 4, 2),
(20, 'Aveo U VA', 4, 1),
(21, 'Beat', 4, 1),
(22, 'Captiva', 4, 4),
(23, 'Cruze', 4, 2),
(24, 'Enjoy', 4, 4),
(25, 'Optra', 4, 2),
(26, 'Optra Magnum', 4, 2),
(27, 'Sail', 4, 2),
(28, 'Spark', 4, 1),
(29, 'Tavera', 4, 2),
(30, 'GO', 5, 1),
(31, 'GO Plus', 5, 1),
(32, 'redi-GO', 5, 1),
(33, 'Grande Punto', 6, 1),
(34, 'Linea', 6, 2),
(35, 'Palio', 6, 1),
(36, 'Palio NV', 6, 1),
(37, 'Punto', 6, 1),
(38, 'Aspire', 7, 2),
(39, 'EcoSport', 7, 14),
(40, 'Endeavour', 7, 4),
(41, 'Fiesta', 7, 2),
(42, 'Fiesta Classic', 7, 2),
(43, 'Figo', 7, 1),
(44, 'Ikon', 7, 2),
(45, 'Accord', 8, 2),
(46, 'Amaze', 8, 2),
(47, 'Brio', 8, 1),
(48, 'BRV', 8, 4),
(49, 'City', 8, 2),
(50, 'City ZX', 8, 2),
(51, 'Civic', 8, 2),
(52, 'CR-V', 8, 4),
(53, 'Jazz', 8, 2),
(54, 'Mobilio', 8, 14),
(55, 'H2', 9, 4),
(56, 'Accent', 10, 2),
(57, 'Creta', 10, 14),
(58, 'Elantra', 10, 2),
(59, 'Elite i20', 10, 1),
(60, 'EON', 10, 1),
(61, 'Getz', 10, 1),
(62, 'Grand i10', 10, 1),
(63, 'i10', 10, 1),
(64, 'i20', 10, 1),
(65, 'i20 Active', 10, 1),
(66, 'Santa Fe', 10, 1),
(67, 'Santro', 10, 1),
(68, 'Santro Xing', 10, 1),
(69, 'Sonata', 10, 2),
(70, 'Sonata Embera', 10, 2),
(71, 'Sonata Transform', 10, 2),
(72, 'Verna', 10, 2),
(73, 'Xcent', 10, 2),
(74, 'XF', 11, 2),
(75, 'XJ', 11, 2),
(76, 'Compass', 12, 4),
(77, 'Rover Discovery Sport', 13, 4),
(78, 'Rover Freelander 2', 13, 4),
(79, 'Rover Range Rover Evoque', 13, 4),
(80, 'Rover Range Rover Sport', 13, 4),
(81, 'Rover Range Rover', 13, 4),
(82, 'Bolero', 14, 4),
(83, 'Jeep', 14, 4),
(84, 'KUV', 14, 14),
(85, 'Logan', 14, 2),
(86, 'Quanto', 14, 14),
(87, 'Renault Logan', 14, 2),
(88, 'Scorpio', 14, 4),
(89, 'Ssangyong Rexton', 14, 4),
(90, 'TUV 300', 14, 14),
(91, 'Verito', 14, 2),
(92, 'XUV500', 14, 4),
(93, 'Xylo', 14, 4),
(94, '800', 15, 1),
(95, 'A Star', 15, 1),
(96, 'Alto', 15, 1),
(97, 'Alto 800', 15, 1),
(98, 'Alto K10', 15, 1),
(99, 'Baleno', 15, 1),
(100, 'Celerio', 15, 1),
(101, 'Ciaz', 15, 2),
(102, 'Dzire', 15, 2),
(103, 'Eeco', 15, 14),
(104, 'Ertiga', 15, 14),
(105, 'Esteem', 15, 2),
(106, 'Grand Vitara', 15, 4),
(107, 'Ignis', 15, 1),
(108, 'Kizashi', 15, 2),
(109, 'Omni', 15, 14),
(110, 'Ritz', 15, 1),
(111, 'S-Cross', 15, 1),
(112, 'Swift', 15, 1),
(113, 'SX4', 15, 2),
(114, 'Versa', 15, 14),
(115, 'Vitara Brezza', 15, 14),
(116, 'Wagon R', 15, 1),
(117, 'Wagon R Stingray', 15, 1),
(118, 'Zen', 15, 1),
(119, 'Zen Estilo', 15, 1),
(120, 'Quattroporte', 16, 2),
(121, 'A-Class', 17, 2),
(122, 'B-Class', 17, 2),
(123, 'C-Class', 17, 2),
(124, 'CLA', 17, 2),
(125, 'CLS', 17, 2),
(126, 'E-Class', 17, 2),
(127, 'GLA Class', 17, 1),
(128, 'GL-Class', 17, 4),
(129, 'GLE', 17, 4),
(130, 'MB Class', 17, 4),
(131, 'M-Class', 17, 4),
(132, 'S-Class', 17, 2),
(133, 'SLC', 17, 2),
(134, 'Viano', 17, 4),
(135, 'Cooper', 18, 1),
(136, 'Cooper 5 DOOR', 18, 1),
(137, 'Cooper Convertible', 18, 1),
(138, 'Countryman', 18, 1),
(139, 'Cedia', 19, 1),
(140, 'Montero', 19, 1),
(141, 'Pajero Sport', 19, 1),
(142, 'Evalia', 20, 14),
(143, 'Micra', 20, 1),
(144, 'Sunny', 20, 2),
(145, 'Teana', 20, 2),
(146, 'Terrano', 20, 4),
(147, 'Astra', 21, 1),
(148, 'Cayenne', 22, 4),
(149, 'Duster', 23, 4),
(150, 'Fluence', 23, 2),
(151, 'KWID', 23, 1),
(152, 'Lodgy', 23, 14),
(153, 'Pulse', 23, 1),
(154, 'Scala', 23, 2),
(155, 'Fabia', 24, 1),
(156, 'Laura', 24, 2),
(157, 'Octavia', 24, 2),
(158, 'Rapid', 24, 1),
(159, 'Superb', 24, 2),
(160, 'Yeti', 24, 1),
(161, 'Aria', 25, 4),
(162, 'Bolt', 25, 1),
(163, 'Hexa', 25, 4),
(164, 'Indica', 25, 1),
(165, 'Indica eV2', 25, 1),
(166, 'Indica V2', 25, 1),
(167, 'Indica V2 Xeta', 25, 1),
(168, 'Indica Vista', 25, 1),
(169, 'Indigo', 25, 2),
(170, 'Indigo eCS', 25, 2),
(171, 'Indigo Marina', 25, 14),
(172, 'Indigo XL', 25, 2),
(173, 'Manza', 25, 2),
(174, 'Nano', 25, 1),
(175, 'Safari', 25, 2),
(176, 'Safari Storme', 25, 2),
(177, 'Sumo Victa DI', 25, 2),
(178, 'Tiago', 25, 1),
(179, 'Vista', 25, 1),
(180, 'Zest', 25, 2),
(181, 'Alphard', 26, 4),
(182, 'Camry', 26, 2),
(183, 'Corolla', 26, 2),
(184, 'Corolla Altis', 26, 2),
(185, 'Etios Cross', 26, 2),
(186, 'Etios Liva', 26, 1),
(187, 'Fortuner', 26, 4),
(188, 'Innova', 26, 4),
(189, 'Innova Crysta', 26, 4),
(190, 'Land Cruiser Prado', 26, 4),
(191, 'Platinum Etios', 26, 2),
(192, 'Qualis', 26, 4),
(193, 'Ameo', 27, 2),
(194, 'Beetle', 27, 1),
(195, 'CrossPolo', 27, 14),
(196, 'Jetta', 27, 2),
(197, 'Passat', 27, 2),
(198, 'Polo', 27, 1),
(199, 'Vento', 27, 2),
(200, 'S60', 28, 2),
(201, 'XC60', 28, 14);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `vehicle_type_id` int(11) NOT NULL,
  `vehicle_type_name` varchar(50) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`vehicle_type_id`, `vehicle_type_name`, `auto_id`) VALUES
(1, 'HATCHBACK', 1),
(2, 'SEDAN', 1),
(3, 'MINI - SUV', 1),
(4, 'SUV', 1),
(5, 'APE', 2),
(6, 'APE METAL BODY', 2),
(7, 'PICK - UP ', 2),
(8, 'PICK - UP METAL BODY', 2),
(9, 'TEMPO 407', 2),
(10, 'TEMPO TRAVELLER', 2),
(11, 'BUS', 2),
(12, 'GEAR', 3),
(13, 'NON-GEAR', 3),
(14, 'MUV', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `automobile`
--
ALTER TABLE `automobile`
  ADD PRIMARY KEY (`auto_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD UNIQUE KEY `vehicle_id` (`vehicle_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD UNIQUE KEY `category_id` (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `customer_id` (`customer_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD UNIQUE KEY `service_id` (`service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_id` (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_number` (`vehicle_number`);

--
-- Indexes for table `vehicle_brand`
--
ALTER TABLE `vehicle_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`vehicle_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `automobile`
--
ALTER TABLE `automobile`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `vehicle_brand`
--
ALTER TABLE `vehicle_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `vehicle_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
