<?php
include_once("config.php");

$extensions= array("jpeg","jpg","png","gif","tif","pdf","doc", "docx", "ppt", "pptx");

if(isset($_POST["business"])){
	 if(isset($_FILES['company_logo'])){
		  $errors="";
		  $file_name = $_FILES['company_logo']['name'];
		  $file_size =$_FILES['company_logo']['size'];
		  $file_tmp =$_FILES['company_logo']['tmp_name'];
		  $file_type=$_FILES['company_logo']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['company_logo']['name'])));
		  		  
		  if(in_array($file_ext,$extensions)=== false){
			 $errors="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size > 5097152){
			 $errors='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp,"upload_images/".time().$file_name);
		 }	
		 $image= time().$file_name;

		 if(isset($_FILES['category_image'])){
		  $errors1="";
		  $file_name1 = $_FILES['category_image']['name'];
		  $file_size1 =$_FILES['category_image']['size'];
		  $file_tmp1 =$_FILES['category_image']['tmp_name'];
		  $file_type1=$_FILES['category_image']['type'];
		  $file_ext1=strtolower(end(explode('.',$_FILES['category_image']['name'])));
		  
		  if(in_array($file_ext1,$extensions)=== false){
			 $errors1="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size1 > 5097152){
			 $errors1='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp1,"upload_images/".time().$file_name1);
		 }
		 $image1=time().$file_name1;
		 
		 if(isset($_FILES['sub_category_image'])){
		  $errors2="";
		  $file_name2=$_FILES['sub_category_image']['name'];
		  $file_size2 =$_FILES['sub_category_image']['size'];
		  $file_tmp2 =$_FILES['sub_category_image']['tmp_name'];
		  $file_type2=$_FILES['sub_category_image']['type'];
		  $file_ext2 =strtolower(end(explode('.',$_FILES['sub_category_image']['name'])));
		  
		  if(in_array($file_ext2,$extensions)=== false){
			 $errors2="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size2 > 5097152){
			 $errors2='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp2,"upload_images/".time().$file_name2);
		 }
		 $image2=time().$file_name2;
		 
		 if(isset($_FILES['cover_image'])){
		  $errors3="";
		  $file_name3=$_FILES['cover_image']['name'];
		  $file_size3=$_FILES['cover_image']['size'];
		  $file_tmp3=$_FILES['cover_image']['tmp_name'];
		  $file_type3=$_FILES['cover_image']['type'];
		  $file_ext3=strtolower(end(explode('.',$_FILES['cover_image']['name'])));
		  
		  if(in_array($file_ext3,$extensions)=== false){
			 $errors3="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size3 > 5097152){
			 $errors3='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp3,"upload_images/".time().$file_name3);
		 }
		 $image3= time().$file_name3;
		 
		 if(isset($_FILES['bg_image'])){
		  $errors4="";
		  $file_name4=$_FILES['bg_image']['name'];
		  $file_size4=$_FILES['bg_image']['size'];
		  $file_tmp4=$_FILES['bg_image']['tmp_name'];
		  $file_type4=$_FILES['bg_image']['type'];
		  $file_ext4=strtolower(end(explode('.',$_FILES['bg_image']['name'])));
		  
		  if(in_array($file_ext4,$extensions)=== false){
			 $errors4="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size4 > 5097152){
			 $errors4='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp4,"upload_images/".time().$file_name4);
		 }
		 $image4=time().$file_name4;
		 
		 if(isset($_FILES['gallery_image'])){
		  $errors5="";
		  $file_name5=$_FILES['gallery_image']['name'];
		  $file_size5=$_FILES['gallery_image']['size'];
		  $file_tmp5=$_FILES['gallery_image']['tmp_name'];
		  $file_type5=$_FILES['gallery_image']['type'];
		  $file_ext5=strtolower(end(explode('.',$_FILES['gallery_image']['name'])));
		  
		  if(in_array($file_ext5,$extensions)=== false){
			 $errors5="extension not allowed, please choose a JPEG or PNG or GIF or TIF file.";
		  }
		  if($file_size5 > 5097152){
			 $errors5='File size must be exactly 5 MB';
		  }
			 move_uploaded_file($file_tmp5,"upload_images/".time().$file_name5);
		 }
		 $image5=time().$file_name5;
	
	/*BASIC DATA*/
	$listing_name=$_POST["listing_name"];
	$listing_tagline=$_POST["tagline"];
	$listing_address=$_POST["address"];
	$listing_phone_no=$_POST["contact"];
	$listing_email=$_POST["email"];
	$listing_website=$_POST["website"];
	$listing_location=$_POST["location"];
	$listing_keywords=$_POST["keywords"];
	$listing_description=$_POST["description"];

	/*SOCIAL PROFILE DATA*/
	$listing_facebook_url=$_POST["facebook"];
	$listing_googleplus_url=$_POST["googleplus"];
	$tab=$_POST["tab"];
	$show1=$_POST["show1"];
	$category=$_POST["listing_category"];
	$sub_category=$_POST["listing_sub_category"];
	$city=$_POST["listing_city"];
	$listing_area=$_POST["listing_area"];

	$sql= "INSERT INTO business(name,tagline,address,contact1,email,website,facebook,googleplus,brouchure,location,description,keyword1,company_logo, cover_image,category_image, gallery_image, background_image, sub_category_image, category_id, sub_category_id, city_id, area_id, show1, tab)VALUES('$listing_name','$listing_tagline','$listing_address','$listing_phone_no','$listing_email','$listing_website','$listing_facebook_url','$listing_googleplus_url','$image1','$listing_location','$listing_description','$listing_keywords','$image','$image3','$image1','$image5','$image4','$image2', '$category', '$sub_category', '$city', '$listing_area', '$show1', '$tab')";
	
	$res =$mysqli->query($sql);
	
	if(!$res)
	{
		echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
	} 
	echo "<script>
			window.alert('Thank You for Adding Your Business..! We will get back to you shortly');
			window.location.href('show-business.php');
		</script>";
}

	$category_name="";
	$sub_category_name="";
	$area_name="";
	
	$sql_q="SELECT * FROM category";
	$res_q=$mysqli->query($sql_q); 
	if(!$res_q)
	{
		echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	
	$sql_i="SELECT * FROM sub_category";
	$res_i=$mysqli->query($sql_i); 
	if(!$res_i)
	{
		echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	
	$sql_w="SELECT * FROM cities";
	$res_w=$mysqli->query($sql_w); 
	if(!$res_w)
	{
		echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	
	$sql_ht="SELECT * FROM area";
	$res_ht=$mysqli->query($sql_ht); 
	if(!$res_ht)
	{
		echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
	}
?>























