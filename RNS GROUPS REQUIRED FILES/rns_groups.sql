-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 19, 2018 at 04:41 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rns_groups`
--

-- --------------------------------------------------------

--
-- Table structure for table `automobile`
--

CREATE TABLE `automobile` (
  `auto_id` int(11) NOT NULL,
  `auto_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `automobile`
--

INSERT INTO `automobile` (`auto_id`, `auto_name`) VALUES
(1, 'car'),
(2, 'transport vehicle'),
(3, 'bike'),
(4, 'Passenger Vehicle');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `service_id` varchar(11) NOT NULL,
  `booking_date` varchar(50) NOT NULL,
  `message` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `customer_id`, `vehicle_id`, `service_id`, `booking_date`, `message`) VALUES
(1, 00159, 18, '5', '09-07-2018 20:10', 'hjg'),
(2, 00154, 15, '6,7,8', '09-07-2018 18:30', 'qwertyui');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(80) NOT NULL,
  `dept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(50) NOT NULL,
  `customer_name` varchar(70) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email_id` varchar(70) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `password` varchar(16) NOT NULL,
  `register_date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `username`, `customer_name`, `address`, `email_id`, `phone`, `password`, `register_date`) VALUES
(00001, 'ajish96', 'Ajish', 'C/02, Alishan CHS', 'www.ajishjoy@gmail.com', '8286541408', '6FAJS12C', ''),
(00154, 'admin', 'Ajish Joy thottungal', 'C/02 Alishan CHS', 'www.ajishjoy@gmail.com', '8286541407', 'ajish@123', ''),
(00155, 'dhawnit', 'dhawnit', 'shahad', 'demo@demo.com', '9702718586', '18HQKGUM', ''),
(00156, 'adminsss', 'Ajish', 'C/02, Alishan CHS', 'a@gmail.com', '8286578407', 'IJ4YW3H1', '20/06/2018'),
(00157, 'arken', 'arken', 'sss', 'qwert', '8286541496', 'H4O6PB58', '27/06/2018'),
(00159, 'vijay93', 'Vijay Jadhav', 'kalyan', 'email@email.com', '9594319954', '3CO6UJQL', '02/07/2018');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `rate_id` int(11) NOT NULL,
  `rate` varchar(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`rate_id`, `rate`, `vehicle_type_id`, `service_id`) VALUES
(1, '50', 1, 1),
(2, '70', 1, 2),
(3, '150', 1, 3),
(4, '200', 1, 4),
(5, '250', 1, 5),
(6, '300', 1, 6),
(7, '300', 1, 7),
(8, '400', 1, 8),
(9, '700', 1, 9),
(10, '800', 1, 10),
(11, '1500', 1, 11),
(12, '1500', 1, 12),
(13, '10000', 1, 13),
(14, '60', 2, 1),
(15, '80', 2, 2),
(16, '200', 2, 3),
(17, '250', 2, 4),
(18, '300', 2, 5),
(19, '350', 2, 6),
(20, '350', 2, 7),
(21, '450', 2, 8),
(22, '800', 2, 9),
(23, '900', 2, 10),
(24, '1800', 2, 11),
(25, '1800', 2, 12),
(26, '15000', 2, 13),
(27, '70', 3, 1),
(28, '90', 3, 2),
(29, '250', 3, 3),
(30, '300', 3, 4),
(31, '350', 3, 5),
(32, '400', 3, 6),
(33, '400', 3, 7),
(34, '500', 3, 8),
(35, '80', 4, 1),
(36, '100', 4, 2),
(37, '300', 4, 3),
(38, '350', 4, 4),
(39, '400', 4, 5),
(40, '450', 4, 6),
(41, '450', 4, 7),
(42, '550', 4, 8),
(43, '900', 4, 9),
(44, '1000', 4, 10),
(45, '2000', 4, 11),
(46, '2000', 4, 12),
(47, '20000', 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(80) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `auto_id`) VALUES
(1, 'WATER WASH', 1),
(2, 'WATER + DRY', 1),
(3, 'SHAMPOO WASH WITHOUT VACCUM', 1),
(4, 'SHAMPOO WASH WITH VACCUM', 1),
(5, 'DIESEL WASH WITHOUT VACCUM', 1),
(6, 'DIESEL WASH WITH VACCUM', 1),
(7, 'SHAMPOO WASH WITH VEHICLE LIFT', 1),
(8, 'DIESEL WASH WITH VEHICLE LIFT', 1),
(9, 'INTERIOR CLEANING', 1),
(10, 'TEFLON POLISH', 1),
(11, 'ENGINE COATING', 1),
(12, 'ANTIRUST COATING', 1),
(13, 'RNS PAINT PROTECTION', 1),
(14, 'WATER WASH', 2),
(15, 'SHAMPOO WASH WITHOUT VACCUM', 2),
(16, 'DIESEL WASH WITHOUT VACCUM', 2),
(17, 'SHAMPOO WASH WITH VEHICLE LIFT', 2),
(18, 'DIESEL WASH WITH VEHICLE LIFT', 2),
(19, 'WATER WASH', 3),
(20, 'WATER + DRY', 3),
(21, 'SHAMPOO WASH', 3),
(22, 'DIESEL WASH ', 3),
(23, 'WATER WASH', 4),
(24, 'WATER + DRY', 4),
(25, 'SHAMPOO WASH', 4),
(26, 'DIESEL WASH ', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(11) NOT NULL,
  `dept` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_type_id` varchar(50) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `make_brand` varchar(50) NOT NULL,
  `vehicle_model` varchar(50) NOT NULL,
  `vehicle_number` varchar(50) NOT NULL,
  `customer_id` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `vehicle_type_id`, `auto_id`, `make_brand`, `vehicle_model`, `vehicle_number`, `customer_id`) VALUES
(23, '15', 4, 'Bajaj', 'RE Compact CNG Auto Rickshaw', 'asdfghjk', '00154');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_brand`
--

CREATE TABLE `vehicle_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_brand`
--

INSERT INTO `vehicle_brand` (`brand_id`, `brand_name`) VALUES
(1, 'Aston'),
(39, 'Atul'),
(2, 'Audi'),
(29, 'Bajaj'),
(3, 'BMW'),
(4, 'Chevrolet'),
(5, 'Datsun'),
(6, 'Fiat'),
(7, 'Ford'),
(30, 'Hero Honda'),
(33, 'Hero MotoCorp'),
(8, 'Honda'),
(9, 'Hummer'),
(10, 'Hyundai'),
(11, 'Jaguar'),
(12, 'Jeep'),
(36, 'KTM'),
(13, 'Land'),
(37, 'LML'),
(40, 'Loha'),
(14, 'Mahindra'),
(15, 'Maruti'),
(16, 'Maserati'),
(17, 'Mercedes-Benz'),
(18, 'Mini'),
(19, 'Mitsubishi'),
(20, 'Nissan'),
(21, 'Opel'),
(38, 'Piaggio\r\n'),
(22, 'Porsche'),
(23, 'Renault'),
(34, 'Royal Enfield'),
(24, 'Skoda'),
(35, 'Suzuki'),
(25, 'Tata'),
(26, 'Toyota'),
(31, 'TVS'),
(27, 'Volkswagen'),
(28, 'Volvo'),
(32, 'Yamaha');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_model`
--

CREATE TABLE `vehicle_model` (
  `model_id` int(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_model`
--

INSERT INTO `vehicle_model` (`model_id`, `model_name`, `brand_id`, `vehicle_type_id`, `auto_id`) VALUES
(1, 'Martin Rapide', 1, 2, 1),
(2, 'A3', 2, 2, 1),
(3, 'A4', 2, 2, 1),
(4, 'A6', 2, 2, 1),
(5, 'A8', 2, 2, 1),
(6, 'Q3', 2, 4, 1),
(7, 'Q5', 2, 4, 1),
(8, 'Q7', 2, 4, 1),
(9, 'S5', 2, 2, 1),
(10, 'TT', 2, 2, 1),
(11, '3 Series', 3, 2, 1),
(12, '5 Series', 3, 2, 1),
(13, 'X4', 3, 4, 1),
(14, '6 Series', 3, 2, 1),
(15, '7 Series', 3, 2, 1),
(16, 'X1', 3, 4, 1),
(17, 'X3', 3, 4, 1),
(18, 'X5', 3, 4, 1),
(19, 'Aveo', 4, 2, 1),
(20, 'Aveo U VA', 4, 1, 1),
(21, 'Beat', 4, 1, 1),
(22, 'Captiva', 4, 4, 1),
(23, 'Cruze', 4, 2, 1),
(24, 'Enjoy', 4, 4, 1),
(25, 'Optra', 4, 2, 1),
(26, 'Optra Magnum', 4, 2, 1),
(27, 'Sail', 4, 2, 1),
(28, 'Spark', 4, 1, 1),
(29, 'Tavera', 4, 4, 1),
(30, 'GO', 5, 1, 1),
(31, 'GO Plus', 5, 1, 1),
(32, 'redi-GO', 5, 1, 1),
(33, 'Grande Punto', 6, 1, 1),
(34, 'Linea', 6, 2, 1),
(35, 'Palio', 6, 1, 1),
(36, 'Palio NV', 6, 1, 1),
(37, 'Punto', 6, 1, 1),
(38, 'Aspire', 7, 2, 1),
(39, 'EcoSport', 7, 14, 1),
(40, 'Endeavour', 7, 4, 1),
(41, 'Fiesta', 7, 2, 1),
(42, 'Fiesta Classic', 7, 2, 1),
(43, 'Figo', 7, 1, 1),
(44, 'Ikon', 7, 2, 1),
(45, 'Accord', 8, 2, 1),
(46, 'Amaze', 8, 2, 1),
(47, 'Brio', 8, 1, 1),
(48, 'BRV', 8, 4, 1),
(49, 'City', 8, 2, 1),
(50, 'City ZX', 8, 2, 1),
(51, 'Civic', 8, 2, 1),
(52, 'CR-V', 8, 4, 1),
(53, 'Jazz', 8, 2, 1),
(54, 'Mobilio', 8, 14, 1),
(55, 'Activa', 8, 13, 3),
(56, 'CB Shine', 8, 12, 3),
(57, 'CB Unicorn', 8, 12, 3),
(58, 'Aviator', 8, 13, 3),
(59, 'Dio', 8, 13, 3),
(60, 'Shine', 8, 12, 3),
(61, 'Unicorn', 8, 12, 3),
(62, 'Dream Yuga', 8, 12, 3),
(63, 'Activa-I', 8, 13, 3),
(64, 'CB Twister', 8, 12, 3),
(65, 'CB HORNET', 8, 12, 3),
(66, 'CB TRIGGER', 8, 12, 3),
(67, 'CB1000R', 8, 12, 3),
(68, 'CBF STUNNER', 8, 12, 3),
(69, 'CBF - 150', 8, 12, 3),
(70, 'CBR 150 R', 8, 12, 3),
(71, 'CBR 400 RR', 8, 12, 3),
(72, 'CBR 600', 8, 12, 3),
(73, 'CBR 650', 8, 12, 3),
(74, 'CBR 929 RR', 8, 12, 3),
(75, 'CBR1000RR', 8, 12, 3),
(76, 'CBR250R', 8, 12, 3),
(77, 'CBX 250', 8, 12, 3),
(78, 'CD 110 DREAM', 8, 12, 3),
(79, 'CG 125', 8, 12, 3),
(80, 'CLIQ', 8, 13, 3),
(81, 'DREAM NEO', 8, 12, 3),
(82, 'ETERNO', 8, 13, 3),
(83, 'GOLD WING', 8, 12, 3),
(84, 'GRAZIA', 8, 13, 3),
(85, 'LIVO', 8, 12, 3),
(86, 'NAVI', 8, 13, 3),
(87, 'RUNE', 8, 12, 3),
(88, 'SCV 100', 8, 13, 3),
(89, 'VFR1200F', 8, 12, 3),
(90, 'WING', 8, 12, 3),
(91, 'X11', 8, 12, 3),
(92, 'X4', 8, 12, 3),
(93, 'XL 1000 V', 8, 12, 3),
(94, 'H2', 9, 4, 1),
(95, 'Accent', 10, 2, 1),
(96, 'Creta', 10, 14, 1),
(97, 'Elantra', 10, 2, 1),
(98, 'Elite i20', 10, 1, 1),
(99, 'EON', 10, 1, 1),
(100, 'Getz', 10, 1, 1),
(101, 'Grand i10', 10, 1, 1),
(102, 'i10', 10, 1, 1),
(103, 'i20', 10, 1, 1),
(104, 'i20 Active', 10, 1, 1),
(105, 'Santa Fe', 10, 1, 1),
(106, 'Santro', 10, 1, 1),
(107, 'Santro Xing', 10, 1, 1),
(108, 'Sonata', 10, 2, 1),
(109, 'Sonata Embera', 10, 2, 1),
(110, 'Sonata Transform', 10, 2, 1),
(111, 'Verna', 10, 2, 1),
(112, 'Xcent', 10, 2, 1),
(113, 'XF', 11, 2, 1),
(114, 'XJ', 11, 2, 1),
(115, 'Compass', 12, 4, 1),
(116, 'Discovery Sport', 13, 4, 1),
(117, 'Freelander 2', 13, 4, 1),
(118, 'Range Rover Evoque', 13, 4, 1),
(119, 'Range Rover Sport', 13, 4, 1),
(120, 'Range Rover', 13, 4, 1),
(121, 'Bolero', 14, 4, 1),
(122, 'Jeep', 14, 4, 1),
(123, 'KUV', 14, 14, 1),
(124, 'Logan', 14, 2, 1),
(125, 'Quanto', 14, 14, 1),
(126, 'Renault Logan', 14, 2, 1),
(127, 'Scorpio', 14, 4, 1),
(128, 'Ssangyong Rexton', 14, 2, 1),
(129, 'TUV 300', 14, 14, 1),
(130, 'Verito', 14, 2, 1),
(131, 'XUV500', 14, 4, 1),
(132, 'Xylo', 14, 4, 1),
(133, 'CENTURO', 14, 12, 3),
(134, 'DURO', 14, 13, 3),
(135, 'GUSTO', 14, 13, 3),
(136, 'RODEO', 14, 13, 3),
(137, 'SYM FLYTE', 14, 13, 3),
(138, 'KINE', 14, 13, 3),
(139, 'MOJO', 14, 12, 3),
(140, 'PANTERO', 14, 12, 3),
(141, 'STTALIO', 14, 12, 3),
(142, 'ZESTO', 14, 13, 3),
(143, 'Alfa DX Auto Rickshaw', 14, 15, 4),
(144, 'Alfa Champ Auto Rickshaw', 14, 15, 4),
(145, 'Alfa Comfy Auto Rickshaw', 14, 15, 4),
(146, 'Alfa Load ', 14, 16, 2),
(147, '800', 15, 1, 1),
(148, 'A Star', 15, 1, 1),
(149, 'Alto', 15, 1, 1),
(150, 'Alto 800', 15, 1, 1),
(151, 'Alto K10', 15, 1, 1),
(152, 'Baleno', 15, 1, 1),
(153, 'Celerio', 15, 1, 1),
(154, 'Ciaz', 15, 2, 1),
(155, 'Dzire', 15, 2, 1),
(156, 'Eeco', 15, 14, 1),
(157, 'Ertiga', 15, 14, 1),
(158, 'Esteem', 15, 2, 1),
(159, 'Grand Vitara', 15, 4, 1),
(160, 'Ignis', 15, 1, 1),
(161, 'Kizashi', 15, 2, 1),
(162, 'Omni', 15, 14, 1),
(163, 'Ritz', 15, 1, 1),
(164, 'S-Cross', 15, 1, 1),
(165, 'Swift', 15, 1, 1),
(166, 'SX4', 15, 2, 1),
(167, 'Versa', 15, 14, 1),
(168, 'Vitara Brezza', 15, 14, 1),
(169, 'Wagon R', 15, 1, 1),
(170, 'Wagon R Stingray', 15, 1, 1),
(171, 'Zen', 15, 1, 1),
(172, 'Zen Estilo', 15, 1, 1),
(173, 'Quattroporte', 16, 2, 1),
(174, 'A-Class', 17, 2, 1),
(175, 'B-Class', 17, 2, 1),
(176, 'C-Class', 17, 2, 1),
(177, 'CLA', 17, 2, 1),
(178, 'CLS', 17, 2, 1),
(179, 'E-Class', 17, 2, 1),
(180, 'GLA Class', 17, 1, 1),
(181, 'GL-Class', 17, 4, 1),
(182, 'GLE', 17, 4, 1),
(183, 'MB Class', 17, 4, 1),
(184, 'M-Class', 17, 4, 1),
(185, 'S-Class', 17, 2, 1),
(186, 'SLC', 17, 2, 1),
(187, 'Viano', 17, 4, 1),
(188, 'Cooper', 18, 1, 1),
(189, 'Cooper 5 DOOR', 18, 1, 1),
(190, 'Cooper Convertible', 18, 1, 1),
(191, 'Countryman', 18, 1, 1),
(192, 'Cedia', 19, 1, 1),
(193, 'Montero', 19, 1, 1),
(194, 'Pajero Sport', 19, 1, 1),
(195, 'Evalia', 20, 14, 1),
(196, 'Micra', 20, 1, 1),
(197, 'Sunny', 20, 2, 1),
(198, 'Teana', 20, 2, 1),
(199, 'Terrano', 20, 4, 1),
(200, 'Astra', 21, 1, 1),
(201, 'Cayenne', 22, 4, 1),
(202, 'Duster', 23, 4, 1),
(203, 'Fluence', 23, 2, 1),
(204, 'KWID', 23, 1, 1),
(205, 'Lodgy', 23, 14, 1),
(206, 'Pulse', 23, 1, 1),
(207, 'Scala', 23, 2, 1),
(208, 'Fabia', 24, 1, 1),
(209, 'Laura', 24, 2, 1),
(210, 'Octavia', 24, 2, 1),
(211, 'Rapid', 24, 1, 1),
(212, 'Superb', 24, 2, 1),
(213, 'Yeti', 24, 1, 1),
(214, 'Aria', 25, 4, 1),
(215, 'Bolt', 25, 1, 1),
(216, 'Hexa', 25, 4, 1),
(217, 'Indica', 25, 1, 1),
(218, 'Indica eV2', 25, 1, 1),
(219, 'Indica V2', 25, 1, 1),
(220, 'Indica V2 Xeta', 25, 1, 1),
(221, 'Indica Vista', 25, 1, 1),
(222, 'Indigo', 25, 2, 1),
(223, 'Indigo eCS', 25, 2, 1),
(224, 'Indigo Marina', 25, 14, 1),
(225, 'Indigo XL', 25, 2, 1),
(226, 'Manza', 25, 2, 1),
(227, 'Nano', 25, 1, 1),
(228, 'Safari', 25, 2, 1),
(229, 'Safari Storme', 25, 2, 1),
(230, 'Sumo Victa DI', 25, 2, 1),
(231, 'Tiago', 25, 1, 1),
(232, 'Vista', 25, 1, 1),
(233, 'Zest', 25, 2, 1),
(234, 'Alphard', 26, 2, 1),
(235, 'Camry', 26, 2, 1),
(236, 'Corolla', 26, 2, 1),
(237, 'Corolla Altis', 26, 2, 1),
(238, 'Etios Cross', 26, 2, 1),
(239, 'Etios Liva', 26, 1, 1),
(240, 'Fortuner', 26, 4, 1),
(241, 'Innova', 26, 4, 1),
(242, 'Innova Crysta', 26, 4, 1),
(243, 'Land Cruiser Prado', 26, 4, 1),
(244, 'Platinum Etios', 26, 2, 1),
(245, 'Qualis', 26, 4, 1),
(246, 'Ameo', 27, 2, 1),
(247, 'Beetle', 27, 1, 1),
(248, 'CrossPolo', 27, 14, 1),
(249, 'Jetta', 27, 2, 1),
(250, 'Passat', 27, 2, 1),
(251, 'Polo', 27, 1, 1),
(252, 'Vento', 27, 2, 1),
(253, 'S60', 28, 2, 1),
(254, 'XC60', 28, 14, 1),
(255, 'PULSAR 150 DTSI', 29, 12, 3),
(256, 'PULSAR 150', 29, 12, 3),
(257, 'DISCOVER 125', 29, 12, 3),
(258, 'AVENGER', 29, 12, 3),
(259, 'DISCOVER', 29, 12, 3),
(260, 'PLATINA', 29, 12, 3),
(261, 'PULSAR 180', 29, 12, 3),
(262, 'PULSAR 135', 29, 12, 3),
(263, 'PULSAR NS', 29, 12, 3),
(264, 'DISCOVER 150', 29, 12, 3),
(265, 'V15', 29, 12, 3),
(266, 'ASPIRE', 29, 12, 3),
(267, 'BOXER', 29, 12, 3),
(268, 'BOXER 150', 29, 12, 3),
(269, 'BRAVO', 29, 13, 3),
(270, 'BYK', 29, 12, 3),
(271, 'CALIBER', 29, 12, 3),
(272, 'CHETAK', 29, 13, 3),
(273, 'CLASSIC', 29, 12, 3),
(274, 'CT 100', 29, 12, 3),
(275, 'CUB', 29, 13, 3),
(276, 'DISCOVER 112', 29, 12, 3),
(277, 'DISCOVER 135 DTSI', 29, 12, 3),
(278, 'DISCOVER F', 29, 12, 3),
(279, 'DISCOVER M', 29, 12, 3),
(280, 'DISCOVER S', 29, 12, 3),
(281, 'DISCOVER ST', 29, 12, 3),
(282, 'DISCOVER T', 29, 12, 3),
(283, 'DOMINAR 400', 29, 12, 3),
(284, 'ELIMINATOR', 29, 12, 3),
(285, 'KAWASAKI NINJA', 29, 12, 3),
(286, 'KB 100', 29, 12, 3),
(287, 'KB 125 RTZ', 29, 12, 3),
(288, 'KRISTAL', 29, 12, 3),
(289, 'LEGENT', 29, 12, 3),
(290, 'M80', 29, 12, 3),
(291, 'PLATINA 125 DTS-I', 29, 12, 3),
(292, 'PRIYA', 29, 13, 3),
(293, 'PULSAR', 29, 12, 3),
(294, 'PULSAR 180', 29, 12, 3),
(295, 'PULSAR 200 DTSI', 29, 12, 3),
(296, 'PULSAR 220 DTSI', 29, 12, 3),
(297, 'PULSAR RS', 29, 12, 3),
(298, 'SAFFIRE', 29, 13, 3),
(299, 'SONIC', 29, 12, 3),
(300, 'SPIRIT', 29, 13, 3),
(301, 'SUNNY', 29, 13, 3),
(302, 'SUPER', 29, 13, 3),
(303, 'SX ENDURO', 29, 12, 3),
(304, 'V12', 29, 12, 3),
(305, 'WAVE', 29, 13, 3),
(306, 'WIND 125', 29, 12, 3),
(307, 'XCD', 29, 12, 3),
(308, 'XCD 125', 29, 12, 3),
(309, 'XCD 135', 29, 12, 3),
(310, 'XCD 135 DTSI', 29, 12, 3),
(311, 'RE Maxima Auto Rickshaw', 29, 15, 4),
(312, 'RE Optima Diesel Auto Rickshaw', 29, 15, 4),
(313, 'RE Optima CNG And LPG Auto Rickshaw', 29, 15, 4),
(314, 'RE Auto Rickshaw Compact 4S', 29, 15, 4),
(315, 'RE Compact LPG Auto Rickshaw', 29, 15, 4),
(316, 'RE Compact Diesel Auto Rickshaw', 29, 15, 4),
(317, 'RE Compact CNG Auto Rickshaw', 29, 15, 4),
(318, 'RE Maxima Cargo', 29, 16, 2),
(319, 'PASSION PRO', 30, 12, 3),
(320, 'SPLENDER PLUSE', 30, 12, 3),
(321, 'PASSION PLUS', 30, 12, 3),
(322, 'CBZ XTREME', 30, 12, 3),
(323, 'CD DELUXE', 30, 12, 3),
(324, 'GLAMOUR', 30, 12, 3),
(325, 'SPENDOR', 30, 12, 3),
(326, 'PLEASURE', 30, 13, 3),
(327, 'SPENDOR PRO', 30, 12, 3),
(328, 'SUPER SPLENDOR', 30, 12, 3),
(329, 'ACHIEVER', 30, 12, 3),
(330, 'AMBITION', 30, 12, 3),
(331, 'CBX', 30, 12, 3),
(332, 'CBZ', 30, 12, 3),
(333, 'CBZ STAR', 30, 12, 3),
(334, 'CD 100', 30, 12, 3),
(335, 'CD DAWN', 30, 12, 3),
(336, 'GLAMOUR FI', 30, 12, 3),
(337, 'HUNK', 30, 12, 3),
(338, 'JOY', 30, 12, 3),
(339, 'KARIZMA', 30, 12, 3),
(340, 'PASSION', 30, 12, 3),
(341, 'SLEEK', 30, 12, 3),
(342, 'SPLENDOR NXG', 30, 12, 3),
(343, 'STREET', 30, 13, 3),
(344, 'APACHE', 31, 12, 3),
(345, 'JUPITER', 31, 13, 3),
(346, 'SCOOTY', 31, 13, 3),
(347, 'WEGO', 31, 13, 3),
(348, 'STAR CITY', 31, 12, 3),
(349, 'SPORTS', 31, 12, 3),
(350, 'VICTOR', 31, 12, 3),
(351, 'XL SUPER', 31, 12, 3),
(352, 'ZEST', 31, 13, 3),
(353, 'PHOENIX', 31, 12, 3),
(354, 'CENTRA', 31, 12, 3),
(355, 'CHAMP', 31, 12, 3),
(356, 'FLAME', 31, 12, 3),
(357, 'JIVE', 31, 12, 3),
(358, 'MAX', 31, 12, 3),
(359, 'MAX 4R', 31, 12, 3),
(360, 'NTORQ', 31, 13, 3),
(361, 'SCOOTY EV', 31, 13, 3),
(362, 'STAR', 31, 12, 3),
(363, 'SUZUKI FIERO', 31, 12, 3),
(364, 'TVS 50', 31, 12, 3),
(365, 'XL', 31, 12, 3),
(366, 'kING 4S LPG, Diesel, Auto Rickshaw?', 31, 15, 4),
(367, 'FZS', 32, 12, 3),
(368, 'FZ 16', 32, 12, 3),
(369, 'FAZER', 32, 12, 3),
(370, 'FZ', 32, 12, 3),
(371, 'YZF R15', 32, 12, 3),
(372, 'RAY ', 32, 13, 3),
(373, 'SZ R', 32, 12, 3),
(374, 'ALPHA', 32, 13, 3),
(375, 'GLADIATOR', 32, 12, 3),
(376, 'SS', 32, 12, 3),
(377, 'XJR 1300', 32, 12, 3),
(378, 'XTZ 125', 32, 12, 3),
(379, 'ALBA 106', 32, 12, 3),
(380, 'CRUX', 32, 12, 3),
(381, 'CRUX R', 32, 12, 3),
(382, 'CYGNUS RAY ZR', 32, 13, 3),
(383, 'ENTICER', 32, 12, 3),
(384, 'ESCORTS ACE', 32, 12, 3),
(385, 'FASCINO', 32, 12, 3),
(386, 'FAZER 25', 32, 12, 3),
(387, 'FZ 1', 32, 12, 3),
(388, 'FZ 25', 32, 12, 3),
(389, 'G5', 32, 12, 3),
(390, 'LIBERO', 32, 12, 3),
(391, 'MT-01', 32, 12, 3),
(392, 'MT-09', 32, 12, 3),
(393, 'RJ', 32, 12, 3),
(394, 'RAJDOOY', 32, 12, 3),
(395, 'RX', 32, 12, 3),
(396, 'RX 135', 32, 12, 3),
(397, 'RXG', 32, 12, 3),
(398, 'RXZ', 32, 12, 3),
(399, 'SALUTO', 32, 12, 3),
(400, 'SZ', 32, 12, 3),
(401, 'SZ RR', 32, 12, 3),
(402, 'SZ S', 32, 12, 3),
(403, 'SZ X', 32, 12, 3),
(404, 'V MAX', 32, 12, 3),
(405, 'YBR', 32, 12, 3),
(406, 'YBR 125', 32, 12, 3),
(407, 'YBX', 32, 12, 3),
(408, 'YD 125', 32, 12, 3),
(409, 'YZF R1', 32, 12, 3),
(410, 'YZF R3', 32, 12, 3),
(411, 'MAESTRO', 33, 13, 3),
(412, 'PASSION PRO', 33, 12, 3),
(413, 'SPLENDOR PRO ', 33, 12, 3),
(414, 'PASSION XPRO', 33, 12, 3),
(415, 'HF DELUXE', 33, 12, 3),
(416, 'SPLENDOR PLUS', 33, 12, 3),
(417, 'GLAMOUR', 33, 12, 3),
(418, 'PLEASURE', 33, 13, 3),
(419, 'SPLENDOR ISMART', 33, 12, 3),
(420, 'IGNITOR', 33, 12, 3),
(421, 'CLASSIC', 34, 12, 3),
(422, 'THUNDERBIRD', 34, 12, 3),
(423, 'ELECTRA', 34, 12, 3),
(424, 'BULLET', 34, 12, 3),
(425, 'CONTINENTAL GT', 34, 12, 3),
(426, 'MACHISMO', 34, 12, 3),
(427, 'HIMALAYAN', 34, 12, 3),
(428, 'INTERCEPTOR', 34, 12, 3),
(429, 'METEOR', 34, 12, 3),
(430, 'CLASSIC 500', 34, 12, 3),
(431, 'TAURUS', 34, 12, 3),
(432, 'ACCESS', 35, 13, 3),
(433, 'GIXXER', 35, 12, 3),
(434, 'SWISH', 35, 12, 3),
(435, 'GS150R', 35, 12, 3),
(436, 'UZ', 35, 13, 3),
(437, 'HAYATE', 35, 12, 3),
(438, 'SLINGSHOT', 35, 12, 3),
(439, 'LETS', 35, 13, 3),
(440, 'ZEUS', 35, 13, 3),
(441, 'HEAT', 35, 12, 3),
(442, 'BANDIT', 35, 12, 3),
(443, 'GIXXER SF FI', 35, 12, 3),
(444, 'GSX', 35, 12, 3),
(445, 'GT 125', 35, 12, 3),
(446, 'HAYABUSA GSX', 35, 12, 3),
(447, 'INAZUMA', 35, 12, 3),
(448, 'IND', 35, 12, 3),
(449, 'INTRUDER', 35, 12, 3),
(450, 'MAX', 35, 12, 3),
(451, 'SAMURAI', 35, 12, 3),
(452, 'SHAOLIN', 35, 12, 3),
(453, 'V-STROM 1000', 35, 12, 3),
(454, 'DUKE', 37, 12, 3),
(455, 'RC', 37, 12, 3),
(456, '690', 37, 12, 3),
(457, 'SUPER DUKE', 37, 12, 3),
(458, 'NV', 36, 12, 3),
(459, 'FREEDOM ', 36, 12, 3),
(460, 'NV 4S', 36, 12, 3),
(461, 'STAR', 36, 12, 3),
(462, 'SELECT II', 36, 12, 3),
(463, 'CRD 100', 36, 12, 3),
(464, 'ENERGY', 36, 12, 3),
(465, 'GRAPTOR', 36, 12, 3),
(466, 'ADRENO', 36, 12, 3),
(467, 'BEAMER', 36, 12, 3),
(468, 'SENSATION', 36, 12, 3),
(469, 'PRITHVI', 36, 12, 3),
(470, 'PLUSE', 36, 12, 3),
(471, 'SUPREMO', 36, 12, 3),
(472, 'TRENDY', 36, 12, 3),
(473, 'SPORTSTER', 37, 12, 3),
(474, 'STREET', 37, 12, 3),
(475, 'FAT BOY', 37, 12, 3),
(476, 'FORTY EIGHT', 37, 12, 3),
(477, 'XL 1200', 37, 12, 3),
(478, 'SUPER GLIDE CUSTOM', 37, 12, 3),
(479, 'ROAD KING', 37, 12, 3),
(480, 'SOFTAIL', 37, 12, 3),
(481, 'FX STANDERD', 37, 12, 3),
(482, 'V-ROD', 37, 12, 3),
(483, 'BREAKOUT', 37, 12, 3),
(484, 'CVO', 37, 12, 3),
(485, 'DYNA ', 37, 12, 3),
(486, 'FAT BOB', 37, 12, 3),
(487, 'NIGHTSTER', 37, 12, 3),
(488, 'Ape City Smart Diesel Auto Rickshaw', 38, 15, 4),
(489, 'Ape City Smart CNG, LPG, PETROL Auto Rickshaw', 38, 15, 4),
(490, 'Ape XTRA DLX Auto Rickshaw', 38, 15, 4),
(491, 'Xtra LD LPG', 38, 16, 2),
(492, 'Xtra HB LPG', 38, 16, 2),
(493, 'Xtra DV LPG', 38, 16, 2),
(494, 'Xtra PV LPG', 38, 16, 2),
(495, 'Xtra LD CNG', 38, 16, 2),
(496, 'Xtra HB CNG', 38, 16, 2),
(497, 'Xtra DV CNG', 38, 16, 2),
(498, 'Xtra PV CNG', 38, 16, 2),
(499, 'Xtra LD Diesel', 38, 16, 2),
(500, 'Xtra HB Diesel', 38, 16, 2),
(501, 'Xtra DV Diesel', 38, 16, 2),
(502, 'Xtra PV Diesel', 38, 16, 2),
(503, 'Gemini-DZ Auto Rickshaw', 39, 15, 4),
(504, 'Gemini-Petrol Auto Rickshaw', 39, 15, 4),
(505, 'Gemini-CNG Auto Rickshaw', 39, 15, 4),
(506, 'Gemini-LPG Auto Rickshaw', 39, 15, 4),
(507, 'GEM Paxx Auto Rickshaw', 39, 15, 4),
(508, 'Shakti Passenger 3 + 1 Rickshaw', 39, 15, 4),
(509, 'Shakti Passenger 6 + 1 Rickshaw', 39, 15, 4),
(510, 'Atul Shakti Pickup Van Standard', 39, 16, 2),
(511, 'Atul GEM Delivery Van', 39, 16, 2),
(512, 'Atul GEM Cargo / Cargo XL', 39, 16, 2),
(513, 'Atul Smart Pickup Van', 39, 16, 2),
(514, 'Humsafar Cargo ', 40, 16, 2);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `vehicle_type_id` int(11) NOT NULL,
  `vehicle_type_name` varchar(50) NOT NULL,
  `auto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`vehicle_type_id`, `vehicle_type_name`, `auto_id`) VALUES
(1, 'HATCHBACK', 1),
(2, 'SEDAN', 1),
(3, 'MINI - SUV', 1),
(4, 'SUV', 1),
(5, 'APE', 2),
(6, 'APE METAL BODY', 2),
(7, 'PICK - UP ', 2),
(8, 'PICK - UP METAL BODY', 2),
(9, 'TEMPO 407', 2),
(10, 'TEMPO TRAVELLER', 2),
(11, 'BUS', 2),
(12, 'GEAR', 3),
(13, 'NON-GEAR', 3),
(14, 'MUV', 1),
(15, 'Autorickshaw', 4),
(16, 'Three Wheeler Tempo', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `automobile`
--
ALTER TABLE `automobile`
  ADD PRIMARY KEY (`auto_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD UNIQUE KEY `vehicle_id` (`vehicle_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD UNIQUE KEY `category_id` (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `customer_id` (`customer_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD UNIQUE KEY `service_id` (`service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_id` (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_number` (`vehicle_number`);

--
-- Indexes for table `vehicle_brand`
--
ALTER TABLE `vehicle_brand`
  ADD PRIMARY KEY (`brand_id`),
  ADD UNIQUE KEY `brand_name` (`brand_name`);

--
-- Indexes for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  ADD PRIMARY KEY (`model_id`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`vehicle_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `automobile`
--
ALTER TABLE `automobile`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `vehicle_brand`
--
ALTER TABLE `vehicle_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  MODIFY `model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;

--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `vehicle_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
