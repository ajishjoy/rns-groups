<?php
 include_once("config.php");
  ?>
		<!DOCTYPE html>

		<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

				<title>RNS GROUPS</title>
				<meta name="keywords" content="" />
				<meta name="description" content="" />

				<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

				<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css"/>
				<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/superfish.css"/>
				<link rel="stylesheet" type="text/css" href="style/flexnav.css"/>
				<link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/layers.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/settings.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/navigation.css"/>
				<link rel="stylesheet" type="text/css" href="style/base.css"/>
				<link rel="stylesheet" type="text/css" href="style/responsive.css"/>

				<script type="text/javascript" src="script/jquery.min.js"></script>

			</head>

			<body class="template-page-book-your-wash">

				<!-- Header -->
				<div class="template-header template-header-background template-header-background-1">

					<!-- Top header -->
					<div class="template-header-top">
            <?php require ('header.php') ?>
					</div>
          <!-- Top header  Content-->
					<div class="template-header-bottom">

						<div class="template-main">

							<div class="template-header-bottom-page-title">
								<h1>Book your wash</h1>
							</div>

							<div class="template-header-bottom-page-breadcrumb">
								<a href="index9ba3.html?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">Book your wash</a>
							</div>

						</div>

					</div>

				</div>

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-component-booking template-section template-main" id="template-booking">

						<!-- Booking form -->
						<form>

							<ul>
			<!-- Step -->


			<!-- Content -->
		<div class="template-component-booking-item-content">

				<!-- Package list -->
				<ul class="template-component-booking-package-list">
				<?php
					$sql_vt="SELECT * FROM vehicle_type";
					$res_vt=$mysqli->query($sql_vt);
					while($row_vt=$res_vt->fetch_assoc()){
					$auto=$row_vt["auto_id"];
				?>
				<li data-id="<?php echo str_replace(" ","_",$row_vt["vehicle_type_name"]);?>" data-id-vehicle-rel="<?php if($auto==1){
					echo "car";
				} else if($auto==2){
					echo "transport_vehicle";
				} else if($auto==3){
					echo "bike";
				}else if($auto==4){
					echo "auto_rickshaw";
				} ?>">

	<!-- Header -->
	<h4 class="template-component-booking-package-name"><?php echo $row_vt["vehicle_type_name"];?></h4>

	<!-- Price -->


	<!-- Duration -->


	<!-- Services -->


	<!-- Button -->
	<div class="template-component-button-box">
		<a href="#" class="template-component-button">Select</a>
	</div>

</li>
<?php } ?>
					<!-- Package -->


				</ul>

			</div>

		</li>
								<!-- Service list -->


		<li>

			<!-- Step -->
			<div class="template-component-booking-item-header template-clear-fix">
				<span>
					<span>3</span>
					<span>/</span>
					<span>4</span>
				</span>
				<h3>Services menu</h3>
				<h5>A la carte services menu.</h5>
			</div>

			<!-- Content -->
			<div class="template-component-booking-item-content">

				<!-- Service list -->
				<ul class="template-component-booking-service-list">

					<?php
						$sql_s="SELECT * FROM service";
						$res_s=$mysqli->query($sql_s);
						while($row_s=$res_s->fetch_assoc()){
						$service=$row_s["auto_id"];
					?>
					<li data-id="<?php echo str_replace(" ","_",$row_s["service_name"]);?>" data-id-vehicle-rel="<?php if($service==1){
						echo "car";
					} else if($service==2){
						echo "transport_vehicle";
					} else if($service==3){
						echo "bike";
					}else if($service==4){
						echo "auto_rickshaw";
					} ?>">

						<div class="template-component-booking-service-name">
							<span><?php echo $row_s["service_name"];?></span>
						</div>
						<div class="template-component-button-box">
							<a href="#" class="template-component-button">Select</a>
						</div>
					</li>
				<?php } ?>

				</ul>

			</div>

		</li>
								<!-- Summary -->


		<li>

			<!-- Step -->
			<div class="template-component-booking-item-header template-clear-fix">
				<span>
					<span>4</span>
					<span>/</span>
					<span>4</span>
				</span>
				<h3>Booking summary</h3>
				<h5>Please provide us with your contact information.</h5>
			</div>


			<!-- Content -->
			<div class="template-component-booking-item-content">


			</div>

			<!-- Content -->
			<div class="template-component-booking-item-content template-margin-top-reset">

				<!-- Layout -->
				<ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">

					<!-- First name -->
					<li class="template-layout-column-left template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-first-name">First Name *</label>
							<input type="text" name="booking-form-first-name" id="booking-form-first-name"/>
						</div>
					</li>

					<!-- Second name -->
					<li class="template-layout-column-right template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-second-name">Second Name *</label>
							<input type="text" name="booking-form-second-name" id="booking-form-second-name"/>
						</div>
					</li>

				</ul>

				<!-- Layout -->
				<ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">

					<!-- E-mail address -->
					<li class="template-layout-column-left template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-email">E-mail Address *</label>
							<input type="text" name="booking-form-email" id="booking-form-email"/>
						</div>
					</li>

					<!-- Phone number -->
					<li class="template-layout-column-right template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-phone">Phone Number *</label>
							<input type="text" name="booking-form-phone" id="booking-form-phone"/>
						</div>
					</li>

				</ul>

				<!-- Layout -->
				<ul class="template-layout-33x33x33 template-layout-margin-reset template-clear-fix">

					<!-- Vehicle make -->
					<li class="template-layout-column-left template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-vehicle-make">Vehicle Make</label>
							<input type="text" name="booking-form-vehicle-make" id="booking-form-vehicle-make"/>
						</div>
					</li>

					<!-- Vehicle model -->
					<li class="template-layout-column-center template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-vehicle-model">Vehicle Model</label>
							<input type="text" name="booking-form-vehicle-model" id="booking-form-vehicle-model"/>
						</div>
					</li>


					<!-- Booking date -->
					<li class="template-layout-column-right template-margin-bottom-reset">
						<div class="template-component-form-field">
							<label for="booking-form-date">Booking Date *</label>
							<input type="text" data-field="datetime" name="booking-form-date" id="booking-form-date"/>
						</div>
					</li>

				</ul>

				<!-- Layout -->
				<ul class="template-layout-100 template-layout-margin-reset template-clear-fix">

					<!-- Message -->
					<li>
						<div class="template-component-form-field">
							<label for="booking-form-message">Message *</label>
							<textarea rows="1" cols="1" name="booking-form-message" id="booking-form-message"></textarea>
						</div>
					</li>

				</ul>

				<!-- Text + submit button -->
				<div class="template-align-center template-clear-fix template-margin-top-2">
					<p class="template-padding-reset template-margin-bottom-2">We will confirm your appointment with you by phone or e-mail within 24 hours of receiving your request.</p>
					<input type="submit" value="Confirm Booking" class="template-component-button" name="booking-form-submit" id="booking-form-submit"/>
					<input type="hidden" value="" name="booking-form-data" id="booking-form-data"/>
				</div>

			</div>

		</li>
							</ul>

						</form>

					</div>

				</div>

				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$('#template-booking').booking();
					});
				</script>

				<!-- Footer -->
        <?php require ('footer.php') ?>

				<!-- Search box -->
				<div class="template-component-search-form">
					<div></div>
					<form>
						<div>
							<input type="search" name="search"/>
							<span class="template-icon-meta-search"></span>
							<input type="submit" name="submit" value=""/>
						</div>
					</form>
				</div>

				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>

				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>

				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				<script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
				<script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>


<!-- Mirrored from quanticalabs.com/Ride N Shine/Template/?page=book-your-wash by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Jun 2018 19:02:26 GMT -->
</html>
