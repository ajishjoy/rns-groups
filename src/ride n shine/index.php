<?php
	$active="1";
?>

		<!DOCTYPE html>
		<html>

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

				<title>RNS GROUPS</title>
				<meta name="keywords" content="" />
				<meta name="description" content="" />

				<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

				<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css"/>
				<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/superfish.css"/>
				<link rel="stylesheet" type="text/css" href="style/flexnav.css"/>
				<link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/layers.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/settings.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/navigation.css"/>
				<link rel="stylesheet" type="text/css" href="style/base.css"/>
				<link rel="stylesheet" type="text/css" href="style/responsive.css"/>

				<script type="text/javascript" src="script/jquery.min.js"></script>

			</head>

			<body class="template-page-home">

				<!-- Header -->
				<div class="template-header">

					<!-- Top header -->
					<?php include_once 'header.php'; ?>
							<div class="template-header-bottom">
						<div id="rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
	<div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
		<ul>
			<!-- Slide 1 -->
			<li data-index="slide-1" data-transition="fade" data-slotamount="1" data-easein="default" data-easeout="default" data-masterspeed="500" data-rotate="0" data-delay="6000">
				<!-- Main image -->
				<img src="media/image/slider/slider_01.jpg" alt="slide-1" data-bgfit="cover" data-bgposition="center bottom">
				<!-- Layers -->
				<!-- Layer 01 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-120','-105','-91','-33','-36']"
					data-fontsize="['17','17','17','15','14']"
					data-fontweight="['700','700','700','700','900']"
					data-lineheight="['17','17','17','15','27']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:[175%];y:0px;z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:0;x:0px;y:0px;z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:[-100%];y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="100"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 2px;"
					>

					WELCOME TO RNS GROUPS
				</div>

				<!-- Layer 02 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-41','-35','-29','17','26']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1000"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					YOUR CAR IS ALWAYS
				</div>

				<!-- Layer 03 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['41','35','29','60','74']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					IN GREAT HANDS WITH US
				</div>

				<!-- Layer 04 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['150','137','121','142','165']"
					data-fontsize="['15','15','15','15','15']"
					data-fontweight="['400','400','400','400','400']"
					data-lineheight="['15','15','15','15','15']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','nowrap']"
					data-width="['auto','auto','auto','auto','auto']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[-100%];z:0px;s:1500;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:750;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="2500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					>


				</div>
			</li>

			<!-- Slide 2 -->
			<li data-index="slide-2" data-transition="fade" data-slotamount="1" data-easein="default" data-easeout="default" data-masterspeed="500" data-rotate="0" data-delay="6000">
				<!-- Main image -->
				<img src="media/image/slider/slider_02.jpg" alt="slide-2" data-bgfit="cover" data-bgposition="center bottom">
				<!-- Layers -->
				<!-- Layer 01 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-120','-105','-91','-33','-36']"
					data-fontsize="['17','17','17','15','14']"
					data-fontweight="['700','700','700','700','900']"
					data-lineheight="['17','17','17','15','27']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:[175%];y:0px;z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:0;x:0px;y:0px;z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:[-100%];y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="100"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 2px;"
					>
					FULL SERVICE AND EXTERIOR TREATMENTS
				</div>

				<!-- Layer 02 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-41','-35','-29','17','26']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1000"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					WE LOVE YOUR CAR
				</div>

				<!-- Layer 03 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['41','35','29','60','74']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					THE SAME AS YOU DO
				</div>

				<!-- Layer 04 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['150','137','121','142','165']"
					data-fontsize="['15','15','15','15','15']"
					data-fontweight="['400','400','400','400','400']"
					data-lineheight="['15','15','15','15','15']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','nowrap']"
					data-width="['auto','auto','auto','auto','auto']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[-100%];z:0px;s:1500;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:750;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="2500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					>


				</div>
			</li>

			<!-- Slide 3 -->
			<li data-index="slide-3" data-transition="fade" data-slotamount="1" data-easein="default" data-easeout="default" data-masterspeed="500" data-rotate="0" data-delay="6000">
				<!-- Main image -->
				<img src="media/image/slider/slider_03.jpg" alt="slide-3" data-bgfit="cover" data-bgposition="center bottom">
				<!-- Layers -->
				<!-- Layer 01 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-120','-105','-91','-33','-36']"
					data-fontsize="['17','17','17','15','14']"
					data-fontweight="['700','700','700','700','900']"
					data-lineheight="['17','17','17','15','27']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:[175%];y:0px;z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:0;x:0px;y:0px;z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:[-100%];y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="100"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 2px;"
					>

					FROM EXPRESS DETAIL TO FULL INSIDE & OUT
				</div>

				<!-- Layer 02 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['-41','-35','-29','17','26']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1000"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					DETAILING SERVICES
				</div>

				<!-- Layer 03 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['41','35','29','60','74']"
					data-fontsize="['62','55','43','29','22']"
					data-fontweight="['900','900','900','700','700']"
					data-lineheight="['62','55','43','29','32']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','normal']"
					data-width="['auto','auto','auto','auto','300']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[100%];z:0px;s:2000;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:1000;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="1500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					style="letter-spacing: 4px;"
					>

					WITH A PERSONAL TOUCH
				</div>

				<!-- Layer 04 -->
				<div class="tp-caption tp-resizeme"
					data-x="['center','center','center','center','center']" data-hoffset="['0','0','0','0','0']"
					data-y="['middle','middle','middle','middle','middle']" data-voffset="['150','137','121','142','165']"
					data-fontsize="['15','15','15','15','15']"
					data-fontweight="['400','400','400','400','400']"
					data-lineheight="['15','15','15','15','15']"
					data-whitespace="['nowrap','nowrap','nowrap','nowrap','nowrap']"
					data-width="['auto','auto','auto','auto','auto']"
					data-height="auto"
					data-basealign="grid"

					data-transform_idle="o:1;"

					data-transform_in="o:1;x:0px;y:[-100%];z:0px;s:1500;e:Power4.easeInOut;"
					data-transform_out="o:1;x:0px;y:[100%];z:0px;s:750;e:Power4.easeInOut;"

					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"

					data-start="2500"

					data-splitin="none"
					data-splitout="none"
					data-responsive_offset="on"
					>


				</div>
			</li>
		</ul>
	</div>
</div>
<!--/-->

<!-- Slider JS -->
<script type="text/javascript">
var tpj=jQuery;
	var revapi4;
	tpj(document).ready(function() {
		if(tpj("#rev_slider").revolution == undefined){
			revslider_showDoubleJqueryError("#rev_slider");
		}else{
			revapi4 = tpj("#rev_slider").show().revolution({
				jsFileLocation:"script/",
				sliderType:"standard",
				sliderLayout:"fullwidth",
				dottedOverlay:"none",
				delay:9000,
				responsiveLevels:[1920,1189,959,767,479],
				gridwidth:[1170,940,750,460,300],
				gridheight:[750,600,550,550,550],
				lazyType:"none",
				navigation: {
					keyboardNavigation:"on",
					mouseScrollNavigation:"off",
					onHoverStop:"on",
					keyboard_direction: "horizontal",
					touch:{
						touchenabled:"on",
						swipe_treshold : 75,
						swipe_min_touches : 1,
						drag_block_vertical:false,
						swipe_direction:"horizontal",
					},
					arrows: {
						style:"preview1",
						enable:true,
						hide_onmobile:true,
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1500,
						hide_under:0,
						hide_over:9999,
						tmp:'',
						left: {
							h_align:"left",
							v_align:"center",
							h_offset:0,
							v_offset:0,
						},
						right: {
							h_align:"right",
							v_align:"center",
							h_offset:0,
							v_offset:0,
						}
					},
					bullets: {
						style:"preview1",
						enable:true,
						hide_onmobile:true,
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1500,
						hide_under:0,
						hide_over:9999,
						direction:"horizontal",
						h_align:"center",
						v_align:"bottom",
						space:10,
						h_offset:0,
						v_offset:20,
						tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
					},
				},
				shadow:0,
				spinner:"spinner1",
				stopAfterLoops:-1,
				stopAtSlide:-1,
				disableProgressBar: "on",
				shuffle:"off",
				autoHeight:"off",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
		});
		}
	}); /*ready*/
</script>					</div>

				</div>

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">

						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Who Is RNS Groups	</h2>
							<div></div>
							<span>Car wash &amp; detailling service</span>
						</div>

						<!-- Layout 33x66% -->
						<div class="template-layout-33x66 template-clear-fix">

							<!-- Left column -->
							<div class="template-layout-column-left">
								<img src="media/image/sample/460x678/image_01.jpg" alt=""/>
							</div>

							<!-- Right column -->
							<div class="template-layout-column-right">

								<!-- Text -->
								<p class="template-padding-reset">
									Ride N Shine Hand Wash is an eco-friendly, hand car wash and detailing service based in Portland.
									Our company was founded back in 2005 by a team of experts with more then 10 years of professional car wash experience.
									We operate three car washes throught Portland area. Our goal is to provide our customers with the friendliest, most convenient
									hand car wash experience possible. We use the most modern and up-to-date water reclamation modules as a part of our car wash systems.
									Our products are all biodegradable and eco-friendly.
								</p>

								<!-- Feature list -->
								<div class="template-component-feature-list template-component-feature-list-position-top template-clear-fix">

									<!-- Layout 50x50% -->
									<ul class="template-layout-50x50 template-clear-fix">

										<!-- Left column -->
										<li class="template-layout-column-left template-margin-bottom-reset">
											<div class="template-component-space template-component-space-2"></div>
											<span class="template-icon-feature-water-drop"></span>
											<h5>The Best Car Wash</h5>
											<ul class="template-component-list">
												<li><span class="template-icon-meta-check"></span>We offer multiple services at a great value</li>
												<li><span class="template-icon-meta-check"></span>Multiple car wash locations throughout Portland</li>
												<li><span class="template-icon-meta-check"></span>Biodegradable and eco-friendly products</li>
												<li><span class="template-icon-meta-check"></span>Pay for your wash electronically and securely</li>
												<li><span class="template-icon-meta-check"></span>Trained and skilled car wash crew members</li>
											</ul>
										</li>

										<!-- Right column -->
										<li class="template-layout-column-right template-margin-bottom-reset">
											<div class="template-component-space template-component-space-2"></div>
											<span class="template-icon-feature-user-chat"></span>
											<h5>Contacting Us</h5>
											<ul class="template-component-list">
												<li><span class="template-icon-meta-check"></span>We are very open and easy to reach company</li>
												<li><span class="template-icon-meta-check"></span>Our email is checked hourly during the day</li>
												<li><span class="template-icon-meta-check"></span>Book an appointment online under 3 minutes</li>
												<li><span class="template-icon-meta-check"></span>Our tool free number will be answered</li>
												<li><span class="template-icon-meta-check"></span>You can pay online for your appointment</li>
											</ul>
										</li>

									</ul>

								</div>

							</div>

						</div>

					</div>

					<div class="template-section template-section-padding-reset template-clear-fix">

							<!-- Flex layout 50x50% -->
							<div class="template-layout-flex template-background-color-1 template-clear-fix">

								<!-- Left column -->
								<div class="template-align-center">

									<!--- Header + subheader -->
									<div class="template-component-header-subheader">
										<h2>Ride N Shine</h2>
										<div></div>
										<span>A great value services</span>
									</div>

									<!-- Text -->
									<p class="template-padding-reset">
										Etiam ullamcorper est terminal metro. Suspendisse a novum etos pellentesque a non felis maecenas module vimeo at malesuada.
										Primus elit lectus at felis malesuada ultricies obec curabitur et ligula sande porta node vestibulum une commodo a convallis laoreet enim.
										Morbi at sinum interdum etos fermentum. Nulla elite terminal integer. Suspendisse a novum etos module un metro.
										<br><br>
										Primus elit lectus at felis malesuada ultricies curabitur et ligula sande. Porta an urna vestibulum commodo convallis laoreet enim.
										Morbi at sinum interdum etos fermentum in dolor supremum.
									</p>

									<!-- Space -->
									<div class="template-component-space template-component-space-2"></div>

									<!-- Button -->
									<a href="booking.php" target="_blank" class="template-component-button">Book Appointment</a>

								</div>

								<!-- Right column -->
								<div class="template-background-image template-background-image-3"></div>

							</div>

							<!-- Flex layout 50x50% -->
							<div class="template-layout-flex template-background-color-1 template-clear-fix">

								<!-- Left column -->
								<div class="template-background-image template-background-image-4"></div>

								<!-- Right column -->
								<div class="template-align-center">

									<!-- Header + subheader -->
									<div class="template-component-header-subheader">
										<h2>Repair n Service</h2>
										<div></div>
										<span>We can deliver the best result</span>
									</div>

									<!-- Text -->
									<p class="template-padding-reset">
										Nunc imperdiet massa eget sollicitudin venenatis ligula nisi porttitor justo eu laoreet libero tellus quis augue.
										Etiam tristique tristique risus nec egestas. Suspendisse pretium rutrum velit et congue tortor blandit in.
										Ut aliquam justo tortor at mollis diam aliquet quis. Nam sodales lobortis enim et accumsan ipsum nullam et scelerisque tellus quis varius orci.
										<br><br>
										Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus.
										Nullam laoreet turpis vitae dapibus mattis turpis odio ultrices mauris eu semper tortor tellus in erat.
									</p>

									<!-- Space -->
									<div class="template-component-space template-component-space-2"></div>

									<!-- Button -->
									<a href="#" class="template-component-button">Coming Soon</a>

								</div>

							</div>

							<div class="template-layout-flex template-background-color-1 template-clear-fix">

								<!-- Left column -->
								<div class="template-align-center">

									<!--- Header + subheader -->
									<div class="template-component-header-subheader">
										<h2>Relax N Security</h2>
										<div></div>
										<span>A great value services</span>
									</div>

									<!-- Text -->
									<p class="template-padding-reset">
										Etiam ullamcorper est terminal metro. Suspendisse a novum etos pellentesque a non felis maecenas module vimeo at malesuada.
										Primus elit lectus at felis malesuada ultricies obec curabitur et ligula sande porta node vestibulum une commodo a convallis laoreet enim.
										Morbi at sinum interdum etos fermentum. Nulla elite terminal integer. Suspendisse a novum etos module un metro.
										<br><br>
										Primus elit lectus at felis malesuada ultricies curabitur et ligula sande. Porta an urna vestibulum commodo convallis laoreet enim.
										Morbi at sinum interdum etos fermentum in dolor supremum.
									</p>

									<!-- Space -->
									<div class="template-component-space template-component-space-2"></div>

									<!-- Button -->
									<a href="#.php" class="template-component-button">Coming Soon</a>

								</div>

								<!-- Right column -->
								<div class="template-background-image template-background-image-3"></div>

							</div>

						</div>

					<!-- Section -->
					<div class="template-section template-background-image template-background-image-5 template-background-image-parallax template-color-white template-clear-fix">

						<!-- Mian -->
						<div class="template-main">

							<!-- Header + subheader -->
							<div class="template-component-header-subheader">
								<h2>Our Process</h2>
								<div></div>
								<span>We know your time is valuable</span>
							</div>

							<!-- Space -->
							<div class="template-component-space template-component-space-1"></div>

							<!-- Process list -->
							<div class="template-component-process-list template-clear-fix">

								<!-- Layout 25x25x25x25% -->
								<ul class="template-layout-25x25x25x25 template-clear-fix template-layout-margin-reset">

									<!-- Left column -->
									<li class="template-layout-column-left">
										<span class="template-icon-feature-check"></span>
										<h5>1. Booking</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>

									<!-- Center left column -->
									<li class="template-layout-column-center-left">
										<span class="template-icon-feature-car-check"></span>
										<h5>2. Inspection</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>

									<!-- Center right column -->
									<li class="template-layout-column-center-right">
										<span class="template-icon-feature-payment"></span>
										<h5>3. Valuation</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>

									<!-- Right column -->
									<li class="template-layout-column-right">
										<span class="template-icon-feature-vacuum-cleaner"></span>
										<h5>4. Completion</h5>
									</li>

								</ul>

							</div>

						</div>


					</div>



					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">

						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-1"></div>

							<!-- Right column -->
							<div class="template-section-padding-1">

								<!-- Features list -->
								<div class="template-component-feature-list template-component-feature-list-position-top">

									<!-- Layout 50x50% -->
									<ul class="template-layout-50x50 template-clear-fix">

										<!-- Left column -->
										<li class="template-layout-column-left">
											<span class="template-icon-feature-location-map"></span>
											<h5>Convenience</h5>
											<p>We are dedicated to providing quality service, customer satisfaction at a great value in multiple locations offering convenient hours.</p>
										</li>

										<!-- Right column -->
										<li class="template-layout-column-right">
											<span class="template-icon-feature-eco-nature"></span>
											<h5>Organic products</h5>
											<p>Our products are eco-friendly and interior products are all organic. We use less than a gallon of water with absolutely zero-waste.</p>
										</li>

										<!-- Left column -->
										<li class="template-layout-column-left">
											<span class="template-icon-feature-team"></span>
											<h5>Experienced Team</h5>
											<p>Our crew members are all trained and skilled and fully equiped with equipment and supplies needed that we can deliver the best results.</p>
										</li>

										<!-- Right column -->
										<li class="template-layout-column-right">
											<span class="template-icon-feature-spray-bottle"></span>
											<h5>Great Value</h5>
											<p>We offer multiple services at a great value to meet your needs. We offer a premium service while saving your time and money.</p>
										</li>

									</ul>

								</div>

							</div>

						</div>

					</div>

					<!-- Section -->
					<div class="template-clear-fix template-main">

						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Latest Projects</h2>
							<div></div>
							<span>Car wash gallery</span>
						</div>


		<!-- Gallery -->
		<div class="template-component-gallery">

			<!-- Filter buttons list -->
			<ul class="template-component-gallery-filter-list">
				<li><a href="#" class="template-filter-all template-state-selected">All</a></li>
				<li><a href="#" class="template-filter-hand-wash">Hand Wash</a></li>
				<li><a href="#" class="template-filter-auto-detail">Auto Detail</a></li>
				<li><a href="#" class="template-filter-triple-foam">Triple Foam</a></li>
				<li><a href="#" class="template-filter-hand-polish">Hand Polish</a></li>
				<li><a href="#" class="template-filter-hand-wax">Hand Wax</a></li>
			</ul>

			<!-- Images list -->
			<ul class="template-component-gallery-image-list">

				<!-- Image -->
				<li class="template-filter-hand-wash">

					<div class="template-component-image template-component-image-preloader">

						<!-- Orginal image -->
						<a href="media/image/sample/460x678/image_01.jpg" class="template-fancybox" data-fancybox-group="gallery-1">

							<!-- Thumbnail -->
							<img src="media/image/sample/460x678/image_01.jpg" alt=""/>

							<!-- Image hover -->
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">BMW i3</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Triple Foam</span>
									</span>
								</span>
							</span>

						</a>

						<!-- Fancybox description -->
						<div class="template-component-image-description">
							Suspendisse sagittis placerat diam sit amet viverra neque elementum et. Donec lacinia in tortor ac tristique.
							In dui leo bibendum et dignissim non laoreet ut nulla. Nunc vulputate odio a dapibus feugiat tortor velit iaculis libero
							sit amet euismod lacus elit et enim. Aliquam semper nunc sed rhoncus interdum.
						</div>

					</div>

				</li>

				<!-- Image -->
				<li class="template-filter-auto-detail template-filter-triple-foam">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/460x306/image_01.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/460x306/image_01.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">Renault Clio</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Hand Polish</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Nam mollis libero at mollis lacinia. Praesent nunc turpis mollis in congue a lacinia at nisl.
							Quisque tincidunt vehicula dolor a eleifend. In nec tellus eu nisi fringilla molestie ac eu enim.
							Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque gravida nibh a lobortis blandit risus turpis auctor ligula.
						</div>
					</div>
				</li>

				<!-- Image -->
				<li class="template-filter-hand-wash template-filter-auto-detail template-filter-hand-wax">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/460x306/image_02.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/460x306/image_02.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">Volkswagen Polo</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Interior Dusting</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Morbi est augue porta in consequat nec pretium eget nunc. In ullamcorper ante quis pharetra finibus.
							Phasellus tincidunt augue sed velit molestie a pretium neque accumsan. Nam id ligula arcu. Proin non tempus eros.
							Pellentesque congue libero eget tristique consectetur.
						</div>
					</div>
				</li>

				<!-- Image -->
				<li class="template-component-gallery-image-list-width-2 template-filter-hand-wash template-filter-triple-foam">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/760x506/image_06.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/760x506/image_06.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">Fiat Bravo</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Cleaning Waterless Wash &amp; Wax with Carnauba</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Donec eget massa vitae metus maximus tempor. Vivamus maximus mattis ultricies. Sed viverra vitae metus in ornare.
							Ut lacus massa finibus quis luctus quis faucibus sit amet risus. Ut elit neque pulvinar sit amet ipsum eget feugiat consequat
							magna. Quisque fringilla ex sit amet rutrum varius velit arcu tempus tellus vitae euismod felis orci vel turpis.
						</div>
					</div>
				</li>

				<!-- Image -->
				<li class="template-filter-triple-foam template-filter-hand-polish">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/460x678/image_03.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/460x678/image_03.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">BMW Mini Cooper</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Wheel Shine</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Phasellus non commodo dolor. In iaculis eleifend ipsum id lacinia turpis sagittis ut. Ut id vestibulum mauris
							ac laoreet augue. Nullam malesuada tempor finibus. Aliquam viverra augue ac tincidunt lacinia. Interdum et malesuada fames
							ac ante ipsum primis in faucibus. Nullam ultricies viverra nulla ac egestas.
						</div>
					</div>
				</li>

				<!-- Image -->
				<li class="template-filter-auto-detail template-filter-hand-polish">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/460x306/image_04.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/460x306/image_04.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">Mazda 3</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Scratch Repair</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Praesent efficitur tortor sit amet nulla malesuada id venenatis velit sagittis. Aliquam blandit ipsum quis iaculis feugiat.
							Sed eros lacus aliquet ut libero et faucibus ultricies nisi. Donec hendrerit dignissim ante vel feugiat augue tempor non.
							Nunc auctor quam sollicitudin est blandit consectetur. Phasellus vitae suscipit purus.
						</div>
					</div>
				</li>

				<!-- Image -->
				<li class="template-filter-triple-foam template-filter-hand-polish template-filter-hand-wax">
					<div class="template-component-image template-component-image-preloader">
						<a href="media/image/sample/460x306/image_10.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
							<img src="media/image/sample/460x306/image_10.jpg" alt=""/>
							<span class="template-component-image-hover">
								<span>
									<span>
										<span class="template-component-image-hover-header">Mercedes A200</span>
										<span class="template-component-image-hover-divider"></span>
										<span class="template-component-image-hover-subheader">Hazy Headlights Restoring</span>
									</span>
								</span>
							</span>
						</a>
						<div class="template-component-image-description">
							Nam et ornare mauris. Nam fringilla eu metus ac vehicula. Mauris dapibus hendrerit ante imperdiet varius orci.
							Fusce rutrum lobortis erat. Nunc vehicula vehicula risus sit amet cursus. Curabitur non congue magna vel porttitor magna.
							Duis interdum ligula sed lectus elementum ut interdum neque dignissim.
						</div>
					</div>
				</li>

			</ul>
							</div>

		</div>



					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">

						<!-- Features list -->
						<div class="template-component-feature-list template-component-feature-list-position-left template-clear-fix">

							<!-- Layout 33x33x33% -->
							<ul class="template-layout-33x33x33 template-clear-fix">

								<!-- Left column -->
								<li class="template-layout-column-left">
									<span class="template-icon-feature-phone-circle"></span>
									<h5>Call Us At</h5>
									<p>
									Phone : 828 604 0687<br/>
									</p>
								</li>

								<!-- Center column -->
								<li class="template-layout-column-center">
									<span class="template-icon-feature-location-map"></span>
									<h5>Our Address</h5>
									<p>
										Mohane Road<br/>
										Opp. Gurudarshan Tower,
										Shahad (W), Kalyan - 421301
									</p>
								</li>

								<!-- Right column -->
								<li class="template-layout-column-right">
									<span class="template-icon-feature-clock"></span>
									<h5>Working hours</h5>
									<p>
										Monday - Friday: 9 am - 6 pm<br/>
										Friday: OFF<br/>
										Saturday - Sunday:  8 am - 6 pm<br/>
									</p>
								</li>

							</ul>
						</div>

					</div>


				<!-- Footer -->
				<?php include_once 'footer.php'; ?>

				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>

				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				<script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
				<script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>


</html>
