
		<!DOCTYPE html>

		<html>



<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

				<title>RNS GROUPS</title>
				<meta name="keywords" content="" />
				<meta name="description" content="" />

				<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

				<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css"/>
				<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/superfish.css"/>
				<link rel="stylesheet" type="text/css" href="style/flexnav.css"/>
				<link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/layers.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/settings.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/navigation.css"/>
				<link rel="stylesheet" type="text/css" href="style/base.css"/>
				<link rel="stylesheet" type="text/css" href="style/responsive.css"/>

				<script type="text/javascript" src="script/jquery.min.js"></script>

			</head>

			<body class="template-page-contact-style-1">

				<!-- Header -->
				<div class="template-header template-header-background template-header-background-1">

					<!-- Top header -->
					<?php include_once("header.php"); ?>
					<!-- Top header -->

					<div class="template-header-bottom">

						<div class="template-main">

							<div class="template-header-bottom-page-title">
								<h1>Contact</h1>
							</div>

							<div class="template-header-bottom-page-breadcrumb">
								<a href="index9ba3.php?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">Contact</a>
							</div>

						</div>

					</div>

				</div>

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-section template-section-padding-1 template-main template-clear-fix">

						<!-- Features list -->
						<div class="template-component-feature-list template-component-feature-list-position-left template-clear-fix">

							<!-- Layout 33x33x33% -->
							<ul class="template-layout-33x33x33 template-clear-fix">

								<!-- Left column -->
								<li class="template-layout-column-left">
									<span class="template-icon-feature-phone-circle"></span>
									<h5>Call Us At</h5>
									<p>
									Phone : 828 604 0687<br/>
									</p>
								</li>

								<!-- Center column -->
								<li class="template-layout-column-center">
									<span class="template-icon-feature-location-map"></span>
									<h5>Our Address</h5>
									<p>
										Mohane Road<br/>
										Opp. Gurudarshan Tower,
										Shahad (W), Kalyan - 421301
									</p>
								</li>

								<!-- Right column -->
								<li class="template-layout-column-right">
									<span class="template-icon-feature-clock"></span>
									<h5>Working hours</h5>
									<p>
										Monday - Friday: 9 am - 6 pm<br/>
										Friday: OFF<br/>
										Saturday - Sunday:  8 am - 6 pm<br/>
									</p>
								</li>

							</ul>

						</div>

					</div>

					<!-- Section -->
					<div class="template-section template-padding-reset template-main template-clear-fix">

						<!-- Contact form -->
						<form name="contact-form" id="contact-form" method="POST" action="#" class="template-component-contact-form">

							<!-- Layout 50x50% -->
							<ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">

								<!-- Left column -->
								<li class="template-layout-column-left template-margin-bottom-reset">
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-name">Your Name *</label>
										<input type="text" name="contact-form-name" id="contact-form-name"/>
									</div>
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-email">Your E-mail *</label>
										<input type="text" name="contact-form-email" id="contact-form-email"/>
									</div>
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-phone">Phone Number</label>
										<input type="text" name="contact-form-phone" id="contact-form-phone"/>
									</div>
								</li>

								<!-- Right column -->
								<li class="template-layout-column-right template-margin-bottom-reset">
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-message">Message *</label>
										<textarea rows="1" cols="1" name="contact-form-message" id="contact-form-message"></textarea>
									</div>
								</li>

							</ul>

							<!-- Button -->
							<div class="template-align-center template-clear-fix template-margin-top-1">
								<span class="template-state-block">
									<input type="submit" value="Submit Message" class="template-component-button" name="contact-form-submit" id="contact-form-submit"/>
								</span>
							</div>

						</form>

						<!-- Space -->
						<div class="template-component-space template-component-space-4"></div>

					</div>



				</div>

				<!-- Footer -->
				<?php include_once("footer.php"); ?>
				<!-- Footer -->
				<!-- Search box -->
				<div class="template-component-search-form">
					<div></div>
					<form>
						<div>
							<input type="search" name="search"/>
							<span class="template-icon-meta-search"></span>
							<input type="submit" name="submit" value=""/>
						</div>
					</form>
				</div>

				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>

				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				<script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
				<script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>



</html>
