<?php
  require('config.php');
  session_start();
  if(isset($_SESSION["uid"]) == null)
  {
    header("location:login.php");
  }

    $sql="SELECT * FROM vehicle WHERE customer_id = '".$_SESSION['uid']."'";
  	$res =$mysqli->query($sql);
    if(!$res)
    {
      echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
    }
          $no_row="";



  ?>
    <!DOCTYPE html>

    <html>

    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- /Added by HTTrack -->

    <head>

        <title>RNS GROUPS</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

        <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
        <link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="style/superfish.css" />
        <link rel="stylesheet" type="text/css" href="style/flexnav.css" />
        <link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/layers.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/settings.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/navigation.css" />
        <link rel="stylesheet" type="text/css" href="style/base.css" />
        <link rel="stylesheet" type="text/css" href="style/responsive.css" />
        <script src="script/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="script/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body class="template-page-book-your-wash">

        <!-- Header -->
        <div class="template-header template-header-background template-header-background-1">

            <!-- Top header -->
            <?php include_once("header.php"); ?>
        </div><br>
				<div class="">

            <!-- Section -->
            <div class="template-component-booking template-section template-main">

                <!-- Booking form -->
                <form action="" method="post">

                    <ul>
                        <!-- Summary -->
                        <li>
                            <br>
                            <!-- Content -->
                            <div class="template-component-booking-item-content template-margin-top-reset">
                              <div class="container">
                                <div class="row">
                                  <?php $i=1;	while ($row= $res->fetch_assoc())
                  										{
                  										$no_row=1;
                                      ?>
                                      <li>
                                        <!-- Step -->
                                        <div class="template-component-booking-item-header template-clear-fix">
                                          <span>
                                            <span class="fa fa-car"></span>
                                          </span>
                                          <h3>Added Vehicles</h3>
                                          <h5>Your Vehicles</h5>
                                        </div>

                                        <!-- Content -->
                                        <div class="template-component-booking-item-content">

                                          <!-- Package list -->
                                          <ul class="template-component-booking-package-list">

                                            <!-- Package -->
                                            <?php $sql="SELECT * FROM vehicle where customer_id=".$_SESSION["uid"];
                                                  $res=$mysqli->query($sql);
                                                  while($row=$res->fetch_assoc())
                                                  { ?>
                                            <li data-id="basic-hand-wash-1" data-id-vehicle-rel="regular-size-car,compact-suv,minivan,pickup-truck,cargo-truck">
                                              <div class="template-component-booking-package-price">
                                                <?php $str = " " ?>
                                                <span class="template-component-booking-package-price-total"><?php echo $row["make_brand"]; ?></span>
                                                <span> <?php echo $row["vehicle_model"]; ?></span>
                                              </div>
                                              <?php $sqll="SELECT * FROM vehicle_type where vehicle_type_id=".$row["vehicle_type_id"];
                                                    $ress=$mysqli->query($sqll);
                                                    $roww=$ress->fetch_assoc();
                                                    ?>
                                              <!-- Header -->
                                              <h4 class="template-component-booking-package-name"><?php echo $roww["vehicle_type_name"]; ?></h4>

                                              <h4 class="template-component-booking-package-name"><?php echo $row["vehicle_number"]; ?></h4>

                                              <!-- Price -->

                                            </li>
                                            <?php } ?>
                                          </ul>
                                        </div>
                                      </li>
                                      <?php
                                      $i++;

                                    }?>
                                  </div>

                                </div>
                              </div>
                              <?php
                                    if($no_row==""){
                                    ?>
                                      <center> OOPS !!! No Vehicle Found, Please <a href="add_vehicle.php">Add Vehicle</a></center>
                                    <?php
                                    } ?>
                                </div>
                        </li>
                    </ul>
                </form>
                </div>
            </div>
          </div>
        <br>
        <!-- Footer -->
        <?php include_once("footer.php"); ?>

            <!-- Go to top button -->
            <a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

            <!-- Wrapper for date picker -->
            <div id="dtBox"></div>

            <!-- JS files -->
            <script type="text/javascript" src="script/jquery-ui.min.js"></script>
            <script type="text/javascript" src="script/superfish.min.js"></script>
            <script type="text/javascript" src="script/jquery.easing.js"></script>
            <script type="text/javascript" src="script/jquery.blockUI.js"></script>
            <script type="text/javascript" src="script/jquery.qtip.min.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox.js"></script>
            <script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
            <script type="text/javascript" src="script/jquery.actual.min.js"></script>
            <script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
            <script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
            <script type="text/javascript" src="script/sticky.min.js"></script>
            <script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
            <script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
            <script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
            <script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
            <script type="text/javascript" src="script/DateTimePicker.min.js"></script>
            <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

            <!-- Revolution Slider files -->
            <script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
            <script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

            <!-- Plugins files -->
            <script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
            <script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
            <script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>
            <!-- <script src="vendor/jquery/dist/jquery.min.js"></script>
            <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script> -->

            <!-- Components files -->
            <script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.image.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.header.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
            <script type="text/javascript" src="script/public.js"></script>

    </body>

    </html>
