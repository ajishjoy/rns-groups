
		<!DOCTYPE html>

		<html>



<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

				<title>RNS GROUPS</title>
				<meta name="keywords" content="" />
				<meta name="description" content="" />

				<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

				<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css"/>
				<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/superfish.css"/>
				<link rel="stylesheet" type="text/css" href="style/flexnav.css"/>
				<link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/layers.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/settings.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/navigation.css"/>
				<link rel="stylesheet" type="text/css" href="style/base.css"/>
				<link rel="stylesheet" type="text/css" href="style/responsive.css"/>

				<script type="text/javascript" src="script/jquery.min.js"></script>

			</head>

			<body class="template-page-about-style-1">

				<!-- Header -->
				<div class="template-header template-header-background template-header-background-2">

					<!-- Top header -->
					<?php include_once("header.php"); ?>

					<div class="template-header-bottom">

						<div class="template-main">

							<div class="template-header-bottom-page-title">
								<h1>About us</h1>
							</div>

							<div class="template-header-bottom-page-breadcrumb">
								<a href="index9ba3.php?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">About us</a>
							</div>

						</div>

					</div>

				</div>

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">

						<!-- Layout 50x50% -->
						<div class="template-layout-50x50 template-clear-fix">

							<!-- Left column -->
							<div class="template-layout-column-left">

								<!-- Header -->
								<div class="template-component-header-subheader template-align-left">
									<h2>RNS GROUPS</h2>
									<div></div>
									<span>Vechicle wash service</span>
								</div>

								<!-- Text -->
								<p class="template-padding-reset">
									We operate three car washes throught Portland area.
									Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.
									We use the most modern and up-to-date water reclamation modules as a part of our car wash systems. Our products are eco-friendly.
								</p>

								<!-- Space -->
								<div class="template-space template-component-space-1"></div>

								<!-- List -->
								<ul class="template-component-list">
									<li>
										<span class="template-icon-meta-check"></span>
										We use professional equipment and have a fully trained staff
									</li>
									<li>
										<span class="template-icon-meta-check"></span>
										We make sure our customers are completely satisfied with their service
									</li>
									<li>
										<span class="template-icon-meta-check"></span>
										Professional detailing will increase the <a href="#">resale value of your car</a>
									</li>
									<li>
										<span class="template-icon-meta-check"></span>
										We are a licensed and fully insured company</li>
									<li>
										<span class="template-icon-meta-check"></span>
										Book and pay for your wash <a href="#">electronically and securely</a>
									</li>
									<li>
										<span class="template-icon-meta-check"></span>
										We are very open and easy to reach company
									</li>
								</ul>

								<!-- Space -->
								<div class="template-space template-component-space-2"></div>

								<!-- Button -->
								<a href="#" class="template-component-button">Our Services</a>

							</div>

							<div class="template-layout-column-right template-margin-bottom-reset">

								<!-- Image -->
								<div class="template-component-image template-component-image-preloader">
									<a href="media/image/sample/760x506/image_06.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
										<img src="media/image/sample/760x506/image_06.jpg" alt=""/>
										<span class="template-component-image-hover"></span>
									</a>
								</div>

								<!-- Space -->

								<!-- Layout 50x50% -->
								<div class="template-layout-50x50 template-clear-fix">

									<!-- Left column -->
									<div class="template-layout-column-left template-margin-bottom-reset">
										<div class="template-component-space template-component-space-1"></div>
										<div class="template-component-image template-component-image-preloader">
											<a href="media/image/sample/760x506/image_09.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
												<img src="media/image/sample/460x306/image_09.jpg" alt=""/>
												<span class="template-component-image-hover"></span>
											</a>
										</div>
									</div>

									<!-- Right column -->
									<div class="template-layout-column-right">
										<div class="template-component-space template-component-space-1"></div>
										<div class="template-component-image template-component-image-preloader">
											<a href="media/image/sample/760x506/image_10.jpg" class="template-fancybox" data-fancybox-group="gallery-1">
												<img src="media/image/sample/460x306/image_10.jpg" alt=""/>
												<span class="template-component-image-hover"></span>
											</a>
										</div>
									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<!-- Footer -->
				<?php include_once("footer.php"); ?>

				<!-- Search box -->
				<div class="template-component-search-form">
					<div></div>
					<form>
						<div>
							<input type="search" name="search"/>
							<span class="template-icon-meta-search"></span>
							<input type="submit" name="submit" value=""/>
						</div>
					</form>
				</div>

				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>

				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				<script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
				<script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>

</html>
