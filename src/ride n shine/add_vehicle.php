<?php
 include_once("config.php");
 session_start();

 if(isset($_POST["submit"])){
   $plate_number = $_POST["plate_number"];
   $radio = $_POST["radio"];

   $sql = "SELECT * FROM vehicle_model where model_id = $radio";
   $res = $mysqli->query($sql);
   $row = $res->fetch_assoc();

   $model_name = $row["model_name"];
   $auto_id = $row["auto_id"];

    $sql_b = "SELECT * FROM vehicle_brand where brand_id =".$row["brand_id"];
    $res_b = $mysqli->query($sql_b);
    $row_b = $res_b->fetch_assoc();

   $make_brand = $row_b["brand_name"];
   $vehicle_type_id = $row["vehicle_type_id"];
   $vehicle_number = $_POST["plate_number"];
   $customer_id = $_SESSION["uid"];

   $sql = "INSERT INTO vehicle (vehicle_type_id,auto_id,make_brand,vehicle_model,vehicle_number,customer_id) VALUES ('$vehicle_type_id','$auto_id','$make_brand','$model_name','$vehicle_number','$customer_id')";
   $res =$mysqli->query($sql);

   if(!$res)
   {
     //echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
     echo "<script>
            window.alert('Vehicle already added.')
            window.location.href='vehicle.php';
           </script>";
   }
   echo "<script>
          window.alert('Vehicle added.')
          window.location.href='vehicle.php';
         </script>";
 }
  ?>
		<!DOCTYPE html>

		<html>
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

				<title>RNS GROUPS</title>
				<meta name="keywords" content="" />
				<meta name="description" content="" />

				<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

        <link href="style/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		    <script src="script/js/bootstrap.js"></script>

				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
				<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

				<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css"/>
				<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/superfish.css"/>
				<link rel="stylesheet" type="text/css" href="style/flexnav.css"/>
				<link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css"/>
				<link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/layers.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/settings.css"/>
				<link rel="stylesheet" type="text/css" href="style/revolution/navigation.css"/>
				<link rel="stylesheet" type="text/css" href="style/base.css"/>
				<link rel="stylesheet" type="text/css" href="style/responsive.css"/>

				<script type="text/javascript" src="script/jquery.min.js"></script>

        <style>
            /* The container */
            .container {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .container input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                margin-left: -25px;
                height: 25px;
                width: 25px;
                background-color: #eee;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked ~ .checkmark {
                background-color: #2196F3;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked ~ .checkmark:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            </style>

			</head>

			<body class="template-page-book-your-wash">

				<!-- Header -->
				<div class="template-header template-header-background template-header-background-1">

					<!-- Top header -->
					<div class="template-header-top">
            <?php require ('header.php') ?>
					</div>

					<div class="template-header-bottom">

						<div class="template-main">

							<div class="template-header-bottom-page-title">
								<h1>Book your wash</h1>
							</div>

							<div class="template-header-bottom-page-breadcrumb">
								<a href="index9ba3.html?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">Book your wash</a>
							</div>

						</div>

					</div>

				</div>

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-component-booking template-section template-main" id="template-booking">

						<!-- Booking form -->
						<form action="" method="post">

							<ul>

								<!-- Vehcile list -->

								<li>

								    <!-- Step -->
								    <div class="template-component-booking-item-header template-clear-fix">
								      <span>
								        <span>1</span>
								        <span>/</span>
								        <span>3</span>
								      </span>
								      <h3>Select Brand</h3>
								      <h5>Select Brand below.</h5>
								    </div>

								    <!-- Content -->
								    <div class="template-component-booking-item-content">

								      <!-- Vehicle list -->
								      <ul class="template-component-booking-vehicle-list">
                        <?php $sql_vb="SELECT * FROM vehicle_brand";
                              $res_vb=$mysqli->query($sql_vb);
                              while($row_vb=$res_vb->fetch_assoc())
                              { ?>
        								        <!-- Vehicle -->
        								        <li data-id="<?php echo $row_vb["brand_id"]; ?>">
        								          <div>
        								            <!-- Icon -->

        								            <div class="template-icon-vehicle-small-car">
                                    </div>
        								            <!-- Name -->
        								            <div><?php echo $brand_name = $row_vb["brand_name"]; ?></div>
        								          </div>
        								        </li>
                          <?php } ?>
                        </ul>
                      </div>


                      <li>

                  			<!-- Step -->
                  			<div class="template-component-booking-item-header template-clear-fix">
                  				<span>
                  					<span>2</span>
                  					<span>/</span>
                  					<span>3</span>
                  				</span>
                  				<h3>Select vehicle model</h3>
                  				<h5>Which Model ?</h5>
                  			</div>

                  			<!-- Content -->
                  			<div class="template-component-booking-item-content">

                  				<!-- Package list -->
                  				<ul class="template-component-booking-package-list">
                  					<!-- Package -->
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=1";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="1">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>

                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=2";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="2">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=3";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="3">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=4";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="4">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=5";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="5">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=6";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="6">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=7";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="7">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=8";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="8">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=9";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="9">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=10";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="10">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=11";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="11">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=12";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="12">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=13";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="13">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=14";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="14">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=15";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="15">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=16";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="16">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=17";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="17">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=18";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="18">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=19";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="19">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=20";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="20">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=21";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="21">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=22";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="22">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=23";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="23">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=24";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="24">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=25";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="25">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=26";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="26">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=27";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="27">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=28";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="28">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=29";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="29">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=30";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="30">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=31";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="31">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=32";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="32">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=33";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="33">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=34";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="34>

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=35";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="35">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=36";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="36">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=37";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="37">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=38";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="38">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=39";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="39">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>
                            <?php $sql_vm="SELECT * FROM vehicle_model WHERE brand_id=40";
                                  $res_vm=$mysqli->query($sql_vm);
                                  while($row_vm=$res_vm->fetch_assoc())
                                  { ?>
                          					<li data-id="basic-hand-wash-1" data-id-vehicle-rel="40">

                          						<!-- Header -->
                          						<h4 class="template-component-booking-package-name"><?php echo $row_vm["model_name"]; ?></h4>
                          						<!-- Button -->
                                      <?php $id = $row_vm["model_id"]; ?>
                                      <label class="container">
                                        <input type="radio" class="" name="radio" value="<?php echo $id ?>">
                                        <span class="checkmark"></span>
                                      </label>

                          					</li>
                            <?php } ?>


                  				</ul>

                  			</div>

                  		</li>

							</ul>

              <li>
                        <!-- Step -->
                        <div class="template-component-booking-item-header template-clear-fix">
                          <span>
                            <span>3</span>
                            <span>/</span>
                            <span>3</span>
                          </span>
                            <h3>Vehicle Plate Number</h3>
                            <h5>Vehicle plate number</h5>
                        </div>
                        <br>
                        <!-- Content -->
                                                <div class="template-component-booking-item-content template-margin-top-reset">
                            <!-- Layout -->
                            <ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">
                                <!-- First name -->
                                <div class="template-component-form-field" style="visibility: visible;">
                                    <label for="booking-form-second-name">Vehicle Plate number *                                     </label>
                                    <div>
                                            <input type="text" class="textbox" id="plate_number" name="plate_number" value="" required="Enter Plate number">
                                    </div>
                                </div>

                            </ul>

                            <!-- Text + submit button -->
                            <div class="template-align-center template-clear-fix template-margin-top-2">
                                <!-- <p class="template-padding-reset template-margin-bottom-2">We will confirm your appointment with you by phone or e-mail within 24 hours of receiving your request.</p> -->
                                <input type="submit" value="Add" class="template-component-button" id="submit" name="submit">
                                <!-- <input type="hidden" value="" name="booking-form-data" id="booking-form-data" /> -->
                            </div>
                            </div>

                    </li>
						</form>

					</div>

				</div>

				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						$('#template-booking').booking();


            $("form").submit(function(){
            if ($('input:radio').filter(':checked').length < 1){
                alert("You have not selected Vehicle Model");
            return false;
            }
          });

					});


				</script>

				<!-- Footer -->
        <?php require ('footer.php') ?>

				<!-- Search box -->
				<div class="template-component-search-form">
					<div></div>
					<form>
						<div>
							<input type="search" name="search"/>
							<span class="template-icon-meta-search"></span>
							<input type="submit" name="submit" value=""/>
						</div>
					</form>
				</div>

				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>

				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>

				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				<script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
				<script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>


<!-- Mirrored from quanticalabs.com/Ride N Shine/Template/?page=book-your-wash by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Jun 2018 19:02:26 GMT -->
</html>
