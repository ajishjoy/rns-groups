<div class="template-footer">

    <div class="template-main">

        <!-- Footer top -->
        <div class="template-footer-top">

            <!-- Layout 25x25x25x25 -->
            <div class="template-layout-25x25x25x25 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left" style="visibility: visible;">
                    <h6>About</h6>
                    <p>RNS GROUPS Hand Wash is an eco-friendly hand car wash and detailing service based in Portland.</p>
                    <img src="media/image/logor.png" alt="" class="template-logo">
                </div>

                <!-- Center left column -->
                <div class="template-layout-column-center-left" style="visibility: visible;">
                    <h6>Services</h6>
                    <ul class="template-list-reset">
                        <li><a href="index371c.html?page=service-style-1">Exterior Hand Wash</a></li>
                        <li><a href="index371c.html?page=service-style-1">Tower Hand Dry</a></li>
                        <li><a href="index371c.html?page=service-style-1">Tire Dressing</a></li>
                        <li><a href="index371c.html?page=service-style-1">Wheel Shine</a></li>
                        <li><a href="index371c.html?page=service-style-1">Interior Vacuum</a></li>
                        <li><a href="index371c.html?page=service-style-1">Sealer Hand Wax</a></li>
                    </ul>
                </div>

                <!-- Center right column -->
                <div class="template-layout-column-center-right" style="visibility: visible;">
                    <h6>Links</h6>
                    <ul class="template-list-reset">
                      <li><a href="index.php">Home</a></li>
                      <li><a href="about_us.php">About Us</a></li>
                      <li><a href="index371c.php">Our Services</a></li>
                      <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right" style="visibility: visible;">
                    <h6>Newsletter</h6>
                    <form class="template-component-newsletter-form">
                        <div class="template-component-form-field template-state-block">
                            <label for="newsletter-form-email">Your e-mail address *</label>
                            <input type="text" name="newsletter-form-email" id="newsletter-form-email">
                        </div>
                        <div class="template-margin-top-2 template-state-block">
                            <input type="submit" value="Subscribe" class="template-component-button" name="newsletter-form-submit" id="newsletter-form-submit">
                        </div>
                    </form>
                </div>

            </div>

        </div>

        <!-- Footer bottom -->
        <div class="template-footer-bottom">

          <!-- Social icon list -->
          <ul class="template-component-social-icon-list template-component-social-icon-list-2">
              <li>
                  <a href="" class="template-icon-social-twitter" target="_blank"></a>
              </li>
              <li>
                  <a href="" class="template-icon-social-facebook" target="_blank"></a>
              </li>
              <li>
                  <a href="" class="template-icon-social-dribbble" target="_blank"></a>
              </li>
              <li>
                  <a href="" class="template-icon-social-envato" target="_blank"></a>
              </li>
              <li>
                  <a href="" class="template-icon-social-behance" target="_blank"></a>
              </li>
              <li>
                  <a href="" class="template-icon-social-youtube" target="_blank"></a>
              </li>
          </ul>

            <!-- Copyright -->
            <div class="template-footer-bottom-copyright">
                By <a href="" target="_blank">RNS Group</a> © 2018 <a href="http://arkeninfotech.com" target="_blank">Developed by Arken Infotech</a>
            </div>

        </div>

    </div>

</div>
