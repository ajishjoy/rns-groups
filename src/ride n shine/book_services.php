<?php
  if(!isset($_SESSION))
  {
      session_start();
  }
  require 'config.php';
  $vehicle_id=$_GET["id"];

  if (isset($_POST["submit"])) {
    $vehicle_id = $_GET["id"];
    $customer_id = $_SESSION["uid"];
    $service_id = implode(',', $_POST['checkbox']);
    $booking_date = $_POST["booking_date"];
    $message = $_POST["message"];

    $sql = "INSERT INTO booking (vehicle_id,customer_id,service_id,booking_date,message) VALUES ('$vehicle_id','$customer_id','$service_id','$booking_date','$message')";
    $res =$mysqli->query($sql);

    if(!$res)
    {
      //echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
      echo "<script>
             window.alert('Vehicle Already Booked.')
             window.location.href='booking.php';
       </script>";
    }
    echo "<script>
           window.alert('Booking successfull.')
           window.location.href='booking_list.php';
          </script>";
  }

 ?>
    <!DOCTYPE html>

    <html>

    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- /Added by HTTrack -->

    <head>

        <title>RNS GROUPS</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

        <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
        <link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="style/superfish.css" />
        <link rel="stylesheet" type="text/css" href="style/flexnav.css" />
        <link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/layers.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/settings.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/navigation.css" />
        <link rel="stylesheet" type="text/css" href="style/base.css" />
        <link rel="stylesheet" type="text/css" href="style/responsive.css" />
        <script src="script/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="script/jquery.min.js"></script>

        <style>
            /* The container */
            .container {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .container input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                right: 0;
                height: 25px;
                width: 25px;
                background-color: #eee;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked ~ .checkmark {
                background-color: #2196F3;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked ~ .checkmark:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            </style>
    </head>

    <body class="template-page-book-your-wash">

      <!-- Header -->
      <div class="template-header template-header-background template-header-background-1">

        <!-- Top header -->
        <div class="template-header-top">
          <?php require ('header.php') ?>
        </div>

        <div class="template-header-bottom">

          <div class="template-main">

            <div class="template-header-bottom-page-title">
              <h1>Book your wash</h1>
            </div>

            <div class="template-header-bottom-page-breadcrumb">
              <a href="index9ba3.html?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">Book your wash</a>
            </div>

          </div>

        </div>

      </div>

      <!-- Content -->
      <div class="template-content">

        <!-- Section -->
        <div class="template-component-booking template-section template-main" id="template-booking">

          <!-- Booking form -->
          <form action="" method="POST">

            <ul>

              <!-- Vehcile list -->

                <li>

                  <!-- Step -->
                  <div class="template-component-booking-item-header template-clear-fix">
                    <span>
                      <span>3</span>
                      <span>/</span>
                      <span>4</span>
                    </span>
                    <h3>Services menu</h3>
                    <h5>A la carte services menu.</h5>
                  </div>

                  <!-- Content -->
                  <div class="template-component-booking-item-content">

                    <!-- Service list -->
                    <ul class="template-component-booking-service-list">

                      <?php  $sqll="SELECT * FROM vehicle where vehicle_id=".$vehicle_id;
                              $ress=$mysqli->query($sqll);
                              $roww=$ress->fetch_assoc();

                        $sql_s="SELECT * FROM service where auto_id=".$roww["auto_id"];
                        $res_s=$mysqli->query($sql_s);
                        while($row_s=$res_s->fetch_assoc()){
                        $service=$row_s["auto_id"];
                      ?>
                      <li data-id="<?php echo str_replace(" ","_",$row_s["service_name"]);?>" data-id-vehicle-rel="<?php if($service==1){
                        echo "car";
                      } else if($service==2){
                        echo "transport_vehicle";
                      } else if($service==3){
                        echo "bike";
                      }else if($service==4){
                        echo "auto_rickshaw";
                      } ?>">

                        <div class="template-component-booking-service-name">
                          <span><?php echo $row_s["service_name"];?></span>
                        </div>

                            <?php $id = $row_s["service_id"];?>
                        <label class="container">
                          <input type="checkbox" class="" name="checkbox[]" value="<?php echo $id ?>">
                          <span class="checkmark"></span>
                        </label>
                      </li>
                    <?php } ?>
                    </ul>

                  </div>

                </li>
                            <!-- Summary -->


                <li>

                  <!-- Step -->
                  <div class="template-component-booking-item-header template-clear-fix">
                    <span>
                      <span>4</span>
                      <span>/</span>
                      <span>4</span>
                    </span>
                    <h3>Booking summary</h3>
                    <h5>Please provide us with your contact information.</h5>
                  </div>

                  <!-- Content -->
                  <li>
                  <div class="template-component-booking-item-content template-margin-top-reset">

                    <!-- Layout -->
                    <ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">
                      <!-- Booking date -->
                      <li class="template-layout-column-full template-margin-bottom-reset">
            						<div class="template-component-form-field">
            							<label for="booking-form-date">Booking Date *</label>
            							<input type="text" data-field="datetime" name="booking_date" id="booking_date" required readonly/>
            						</div>
            					</li>

                    </ul>

                    <!-- Layout -->
                    <ul class="template-layout-100 template-layout-margin-reset template-clear-fix">

                      <!-- Message -->
                      <li>
                        <div class="template-component-form-field">
                          <label for="booking-form-message">Message </label>
                          <textarea rows="1" cols="1" name="message" id="message" ></textarea>
                        </div>
                      </li>

                    </ul>

                    <!-- Text + submit button -->
                    <div class="template-align-center template-clear-fix template-margin-top-2">
                      <!-- <p class="template-padding-reset template-margin-bottom-2">We will confirm your appointment with you by phone or e-mail within 24 hours of receiving your request.</p> -->
                      <input type="submit" value="Confirm Booking" class="template-component-button" name="submit" id="submit"/>
                      <input type="hidden" value="" name="booking-form-data" id="booking-form-data"/>
                    </div>

                  </div>

                </li>
            </ul>

          </form>

        </div>

      </div>

      <script type="text/javascript">
        jQuery(document).ready(function($)
        {
          $("form").submit(function(){
          if ($('input:checkbox').filter(':checked').length < 1){
              alert("You have not selected any Services");
          return false;
          }
        });

        $('#booking-form-date').on('focus',function(e)
        {
            $(this).blur();
        });
          $("#dtBox").DateTimePicker(
          {
            dateTimeFormat		:	'dd-MM-yyyy hh:mm',
            animationDuration	:	800,
            minuteInterval		:	10,
            buttonsToDisplay	:	['SetButton','ClearButton']
          });

        });
      </script>



      <!-- Footer -->
      <?php require ('footer.php') ?>

      <!-- Search box -->
      <div class="template-component-search-form">
        <div></div>
        <form>
          <div>
            <input type="search" name="search"/>
            <span class="template-icon-meta-search"></span>
            <input type="submit" name="submit" value=""/>
          </div>
        </form>
      </div>

      <!-- Go to top button -->
      <a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

      <!-- Wrapper for date picker -->
      <div id="dtBox"></div>

      <!-- JS files -->
      <script type="text/javascript" src="script/jquery-ui.min.js"></script>
      <script type="text/javascript" src="script/superfish.min.js"></script>
      <script type="text/javascript" src="script/jquery.easing.js"></script>
      <script type="text/javascript" src="script/jquery.blockUI.js"></script>
      <script type="text/javascript" src="script/jquery.qtip.min.js"></script>
      <script type="text/javascript" src="script/jquery.fancybox.js"></script>
      <script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
      <script type="text/javascript" src="script/jquery.actual.min.js"></script>
      <script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
      <script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
      <script type="text/javascript" src="script/sticky.min.js"></script>
      <script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
      <script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
      <script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
      <script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
      <script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
      <script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
      <script type="text/javascript" src="script/DateTimePicker.min.js"></script>

      <!-- Revolution Slider files -->
      <script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
      <script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
      <script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

      <!-- Plugins files -->
      <script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
      <script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
      <script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>

      <!-- Components files -->
      <script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.image.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.header.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
      <script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
      <script type="text/javascript" src="script/public.js"></script>

    </body>

    </html>
