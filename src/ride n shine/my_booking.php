<?php
  session_start();
  if(isset($_SESSION["uid"]) == null)
  {
    header("location:login.php");
  }
 ?>
<!DOCTYPE html>

<html>

<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>

    <title>RNS GROUPS</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link href="style/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="script/js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

    <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="style/superfish.css" />
    <link rel="stylesheet" type="text/css" href="style/flexnav.css" />
    <link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css" />
    <link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css" />
    <link rel="stylesheet" type="text/css" href="style/revolution/layers.css" />
    <link rel="stylesheet" type="text/css" href="style/revolution/settings.css" />
    <link rel="stylesheet" type="text/css" href="style/revolution/navigation.css" />
    <link rel="stylesheet" type="text/css" href="style/base.css" />
    <link rel="stylesheet" type="text/css" href="style/responsive.css" />
    <script src="script/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="script/jquery.min.js"></script>

</head>

<body class="template-page-book-your-wash">

    <!-- Header -->
    <div class="template-header template-header-background template-header-background-1">

        <!-- Top header -->
        <?php include_once("header.php"); ?>
        <div class="template-header-bottom">

        <div class="template-main">

          <div class="template-header-bottom-page-title">
            <h1>Your Booking</h1>
          </div>

          <div class="template-header-bottom-page-breadcrumb">
            <a href="index9ba3.html?page=home">Home</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">Login</a><span class="template-icon-meta-arrow-right-12"></span><a href="#">My Booking</a>
          </div>

        </div>

      </div>
    </div><br>
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="template-component-announcement">
            <i class="far fa-calendar-check"></i>
            <h3>Book now</h3>
             <div class="template-component-italic">Book for wash</div>
             <a href="booking.php" class="template-component-button">Book</a>
           </div>
          </div>
          <div class="col-sm-4">
            <div class="template-component-announcement">
              <h3>Your Booking</h3>
               <div class="template-component-italic">Your booking list</div>
               <a href="booking_list.php" class="template-component-button">Show list</a>
             </div>
            </div>

          </div><br>
        </div>

    <!-- Footer -->
    <?php include_once("footer.php"); ?>

        <!-- Go to top button -->
        <a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

        <!-- Wrapper for date picker -->
        <div id="dtBox"></div>

        <!-- JS files -->
        <script type="text/javascript" src="script/jquery-ui.min.js"></script>
        <script type="text/javascript" src="script/superfish.min.js"></script>
        <script type="text/javascript" src="script/jquery.easing.js"></script>
        <script type="text/javascript" src="script/jquery.blockUI.js"></script>
        <script type="text/javascript" src="script/jquery.qtip.min.js"></script>
        <script type="text/javascript" src="script/jquery.fancybox.js"></script>
        <script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="script/jquery.actual.min.js"></script>
        <script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
        <script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="script/sticky.min.js"></script>
        <script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
        <script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
        <script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
        <script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
        <script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="script/DateTimePicker.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

        <!-- Revolution Slider files -->
        <script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

        <!-- Plugins files -->
        <script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
        <script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
        <script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>
        <!-- <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script> -->

        <!-- Components files -->
        <script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.image.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.header.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
        <script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
        <script type="text/javascript" src="script/public.js"></script>

</body>

</html>
