<?php
  require 'config.php';

  session_start();
  if(isset($_SESSION["uid"]) == null)
  {
    header("location:login.php");
  }

 ?>
    <!DOCTYPE html>

    <html>

    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- /Added by HTTrack -->

    <head>

        <title>RNS GROUPS</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

        <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
        <link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="style/superfish.css" />
        <link rel="stylesheet" type="text/css" href="style/flexnav.css" />
        <link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/layers.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/settings.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/navigation.css" />
        <link rel="stylesheet" type="text/css" href="style/base.css" />
        <link rel="stylesheet" type="text/css" href="style/responsive.css" />
        <script src="script/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="script/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body class="template-page-book-your-wash">

          <!-- Header -->
          <div class="template-header template-header-background template-header-background-1">

              <!-- Top header -->
              <div class="template-header-top">
                <?php include_once("header.php"); ?>
              </div>
              </div>

              <!-- Content -->
        <div class="">
          <!-- Section -->
          <div class="template-component-booking template-section template-main" id="template-booking" >
              <!-- Content -->
              <div class="template-component-booking template-section template-main">

                  <!-- Booking form -->
                  <form action="" method="post">

                      <ul>
                          <!-- Summary -->
                          <li>
                              <br>
                              <!-- Content -->
                              <div class="template-component-booking-item-content template-margin-top-reset">
                                <div class="container">
                                  <div class="row">

                                        <li>
                                          <!-- Step -->
                                          <div class="template-component-booking-item-header template-clear-fix">
                                            <span>
                                              <span class="fa fa-car"></span>
                                            </span>
                                            <h3>Booking List</h3>
                                            <h5>Vehicle booked for wash</h5>
                                          </div>

                                          <!-- Content -->
                                          <div class="template-component-booking-item-content">

                                            <!-- Package list -->
                                            <ul class="template-component-booking-package-list">
                                              <?php
                                                $sql_b ="SELECT * FROM booking where customer_id =".$_SESSION["uid"];
                                                  $res_b = $mysqli->query($sql_b);
                                                  while($row_b = $res_b->fetch_assoc()){
                                                   ?>
                                              <li data-id="basic-hand-wash-1" data-id-vehicle-rel="regular-size-car,compact-suv,minivan,pickup-truck,cargo-truck">
                                                <?php
                                                  $sql_v ="SELECT * FROM vehicle where vehicle_id =".$row_b["vehicle_id"];
                                                    $res_v = $mysqli->query($sql_v);
                                                    while($row_v = $res_v->fetch_assoc()){
                                                     ?>
                                                     <div class="template-component-booking-package-price">
                                                       <?php $str = " "; ?>
                                                       <span class="template-component-booking-package-price-total"><?php echo $row_v["make_brand"]?></span>
                                                       <span><?php echo $row_v["vehicle_model"]?></span>
                                                     </div>
                                                <h4 class="template-component-booking-package-name"><?php echo $row_v["vehicle_number"] ?></h4>
                                              <?php } ?>

                                              <?php

                                              $myString = $row_b["service_id"];
                                              $myArray = explode(',', $myString);
                                              foreach($myArray as $my_Array){

                                                $sql_s="select * from Service where service_id=".$my_Array;
                                                $res_s=$mysqli->query($sql_s);
                                                $row_s=$res_s->fetch_assoc();

                                                  echo $row_s["service_name"],'<br>';
                                              }
                                            //  print_r($myArray);

                                              ?>
                                              <h4 class="template-component-booking-package-name"><?php ?></h4>

                                              <h4 class="template-component-booking-package-name"><?php echo $row_b["booking_date"]?></h4>
                                              <div class="template-component-button-box" id="demo">
                                                <a type="submit" href="cancel_booking.php?id=<?php echo $row_b["vehicle_id"];?>" class="template-component-button" name="delete" onclick="return confirm('Are you sure to cancel this booking')">Cancel this booking</a>
                                              </div>
                                              </li>
                                              <?php } ?>
                                            </ul>
                                          </div>
                                          <?php
                                            $sql_bb ="SELECT * FROM booking where customer_id =".$_SESSION["uid"];
                                              $res_bb = $mysqli->query($sql_bb);
                                              $row_bb = $res_bb->fetch_assoc();
                                              if ($row_bb["vehicle_id"] == null) { ?>
                                                <h4 class="template-component-booking-package-name">No Bookings Found<a href="booking.php"> Book for wash</a></h4>
                                              <?php } ?>
                                        </li>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

          <!-- Footer -->
          <?php include_once("footer.php"); ?>

              <!-- Go to top button -->
              <a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

              <!-- Wrapper for date picker -->
              <div id="dtBox"></div>

              <!-- JS files -->
              <script type="text/javascript" src="script/jquery-ui.min.js"></script>
              <script type="text/javascript" src="script/superfish.min.js"></script>
              <script type="text/javascript" src="script/jquery.easing.js"></script>
              <script type="text/javascript" src="script/jquery.blockUI.js"></script>
              <script type="text/javascript" src="script/jquery.qtip.min.js"></script>
              <script type="text/javascript" src="script/jquery.fancybox.js"></script>
              <script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
              <script type="text/javascript" src="script/jquery.actual.min.js"></script>
              <script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
              <script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
              <script type="text/javascript" src="script/sticky.min.js"></script>
              <script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
              <script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
              <script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
              <script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
              <script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
              <script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
              <script type="text/javascript" src="script/DateTimePicker.min.js"></script>
              <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

              <!-- Revolution Slider files -->
              <script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
              <script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
              <script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

              <!-- Plugins files -->
              <script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
              <script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
              <script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>
              <!-- <script src="vendor/jquery/dist/jquery.min.js"></script>
              <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script> -->

              <!-- Components files -->
              <script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.image.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.header.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
              <script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
              <script type="text/javascript" src="script/public.js"></script>

      </body>

    </html>
