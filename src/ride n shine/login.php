<?php
	require ("config.php");

	session_start();
	if(isset($_SESSION["uid"]))
	{
		header("location:dashboard.php");
	}

	$errstatus=0;
	$errmsg="";
	if(isset($_POST["submit"]))
	{
		$username=$_POST["username"];
		if($username=="")
		{
			$errstatus=1;
			$errmsg=$errmsg."Please enter Username <br />";
		}
		$password=$_POST["password"];
		if($password=="")
		{
			$errstatus=1;
			$errmsg=$errmsg."Please enter password <br />";
		}
		if($errstatus!=1)
		{
			$sql="SELECT  * FROM  customer WHERE username='".$username."' AND password='".$password."'";
			$res =$mysqli->query($sql);

			if(!$res)
			{
				echo "Error: (" . $mysqli->errno . ") " . $mysqli->error;
				die();
			}
			/*-------------- Fetching row if that user exist--------------------*/
			if($res->num_rows==1)
			{
				/*--------------set a session-----------*/
				$row= $res->fetch_assoc();

				$_SESSION["uid"]=$row["customer_id"];
				$_SESSION["username"] = $username;

				/*--------------Redirected-----------*/
				header("location:dashboard.php");
			}
			else
			{
				$errstatus=1;
				$errmsg=$errmsg."Invalid User name and password.";
			}
		}
	}
?>
    <!DOCTYPE html>

    <html>

    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- /Added by HTTrack -->

    <head>

        <title>RNS GROUPS</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Serif:700italic,700,400italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic">

        <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
        <link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="style/superfish.css" />
        <link rel="stylesheet" type="text/css" href="style/flexnav.css" />
        <link rel="stylesheet" type="text/css" href="style/DateTimePicker.min.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" type="text/css" href="style/fancybox/helpers/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/layers.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/settings.css" />
        <link rel="stylesheet" type="text/css" href="style/revolution/navigation.css" />
        <link rel="stylesheet" type="text/css" href="style/base.css" />
        <link rel="stylesheet" type="text/css" href="style/responsive.css" />
        <script src="script/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="script/jquery.min.js"></script>
    </head>

    <body class="template-page-book-your-wash">

        <!-- Header -->
        <div class="template-header template-header-background template-header-background-1">

            <!-- Top header -->
            <?php include_once("header.php"); ?>
        </div>
        <br>


				<div class="template-content">

            <!-- Section -->
            <div class="template-component-booking template-section template-main">

                <!-- Booking form -->
                <form action="" method="post">

                    <ul>
                        <!-- Summary -->
                        <li>
                            <!-- Step -->
                            <div class="template-component-booking-item-header template-clear-fix">
                                <span>
                                <span>1</span>
                                <span>/</span>
                                <span>1</span>
                                </span>
                                <h3>Sign In</h3>
                                <h5>Sign in with your credentials</h5>
																<p style="margin: 0px 0px 0px 110px;">Do not have account then <a href="user_registration.php" class="forgot-link">Register Here</a></p>

																<?php if($errstatus==1){ ?>
																		<div class="widget-block-title">
																				<p><font color="red"><?php echo $errmsg;?></font></p>
																		</div>
																		<?php } ?>
                            </div>
                            <br>
                            <!-- Content -->
                            <div class="template-component-booking-item-content template-margin-top-reset">
                              <!-- Layout -->
                              <ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">
                                <!-- First name -->
                                <li class="template-layout-column-left template-margin-bottom-reset">
                                	<div class="template-component-form-field">
                                  	<label for="booking-form-first-name">User Name</label>
                                    <input type="text" name="username" id="username" required/>
                                  </div>
                                </li>
                                    <!-- Second name -->
                                <li class="template-layout-column-right template-margin-bottom-reset">
                                	<div class="template-component-form-field">
                                    <label for="booking-form-second-name">Password</label>
																			<input type="password" class="password" id="password" name="password" required/>
																	</div>
                              	</li>
                                </ul>
															</div><br>
																<center><a href="forgot_password.php" class="forgot-link">Forgotten account ?</a></center>
                                <!-- Text + submit button -->
                                <div class="template-align-center template-clear-fix template-margin-top-2">
                                    <!-- <p class="template-padding-reset template-margin-bottom-2">We will confirm your appointment with you by phone or e-mail within 24 hours of receiving your request.</p> -->
                                    <input type="submit" value="Sign in" class="template-component-button" name="submit" />
                                    <!-- <input type="hidden" value="" name="booking-form-data" id="booking-form-data" /> -->
                                </div>
                                </div>
                        </li>
                    </ul>
                </form>
                </div>
            </div>
          </div>
        <br>
        <!-- Footer -->
        <?php include_once("footer.php"); ?>

            <!-- Go to top button -->
            <a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>

            <!-- Wrapper for date picker -->
            <div id="dtBox"></div>

            <!-- JS files -->
            <script type="text/javascript" src="script/jquery-ui.min.js"></script>
            <script type="text/javascript" src="script/superfish.min.js"></script>
            <script type="text/javascript" src="script/jquery.easing.js"></script>
            <script type="text/javascript" src="script/jquery.blockUI.js"></script>
            <script type="text/javascript" src="script/jquery.qtip.min.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox.js"></script>
            <script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
            <script type="text/javascript" src="script/jquery.actual.min.js"></script>
            <script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
            <script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
            <script type="text/javascript" src="script/sticky.min.js"></script>
            <script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
            <script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
            <script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
            <script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
            <script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
            <script type="text/javascript" src="script/DateTimePicker.min.js"></script>
            <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>

            <!-- Revolution Slider files -->
            <script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
            <script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
            <script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

            <!-- Plugins files -->
            <script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
            <script type="text/javascript" src="plugin/contact-form/jquery.contactForm.js"></script>
            <script type="text/javascript" src="plugin/newsletter-form/jquery.newsletterForm.js"></script>
            <!-- <script src="vendor/jquery/dist/jquery.min.js"></script>
            <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script> -->

            <!-- Components files -->
            <script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.image.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.header.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
            <script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
            <script type="text/javascript" src="script/public.js"></script>

    </body>

    </html>
