<?php
	session_start();
	session_destroy();
	unset($_SESSION["uid"]);
	unset($_SESSION["username"]);
	header("location:index.php?msg=You%20have%20been%20logged%20out!&type=information");
?>