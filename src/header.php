<div class="template-header-top">

    <!-- Logo -->
    <div class="template-header-top-logo">
        <a href="index.php" title="">
            <img src="media/image/logor.png" alt="" class="template-logo">
            <img src="media/image/logor.png" alt="" class="template-logo template-logo-sticky">
        </a>
    </div>

    <!-- Menu-->
    <div class="template-header-top-menu template-main">
        <nav>
            <!-- Default menu-->
            <div class="template-component-menu-default">
                <ul class="sf-menu sf-js-enabled" style="touch-action: pan-y;">
                    <li><a href="index.php" class="<?php if ($active==1) { echo " template-state-selected "; } else {} ?>">Home</a></li>
                    <li><a href="about_us.php">About Us</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>

        </nav>
        <nav>

            <!-- Mobile menu-->
            <div class="template-component-menu-responsive">
              <ul class="flexnav">
                <li><a href="#"><span class="touch-button template-icon-meta-arrow-large-tb template-component-menu-button-close"></span>&nbsp;</a></li>
                <li><a href="index.php" class="<?php if ($active==1) { echo " template-state-selected "; } else {} ?>">Home</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="contact.php">Contact</a></li>
              </ul>
            </div>





        </nav>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.template-header-top').templateHeader();
            });
        </script>
    </div>

    <!-- Social icons -->
    <div class="template-header-top-icon-list template-component-social-icon-list-1">
        <ul class="template-component-social-icon-list">
          <li>
              <a href="#" class="template-icon-meta-menu"></a>
          </li>
            <li>
                <a href="" class="template-icon-social-twitter" target="_blank"></a>
            </li>
            <li>
                <a href="" class="template-icon-social-facebook" target="_blank"></a>
            </li>
            <li>
                <a href="" class="template-icon-social-dribbble" target="_blank"></a>
            </li>
        </ul>
    </div>
</div>
